# README #

These are my MVC project for office application, the main purpose is as my backup environment. I do not provide the DB table schema and the DBconnection strings are for my local Len only. If you wanna to reference the UI interface, you may check my other brief introdution http://danielchou.github.io . 

### What are these project? ###
* TimeSheet - a system to statistic man hour by project or department. 
* Commenting - inspire by DISQUS, a comment module for a link to plugin at any website.
* FormMaker - inspire by Google Form, a customised questionnaire system.
* StaffSoftwareQuery - software query system, data provide from PowerShell.