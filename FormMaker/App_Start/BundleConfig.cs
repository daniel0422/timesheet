﻿using System.Web;
using System.Web.Optimization;

namespace FormMaker {
    public class BundleConfig {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles) {
            bundles.Add(new ScriptBundle("~/requirejs").Include(
                        "~/Scripts/require.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                        "~/Public/js/vendor/jquery/jquery.js",
                        "~/Public/js/vendor/jqueryui/jquery-ui.js",
                        "~/Public/js/vendor/semantic-ui/dist/semantic.js",
                        "~/Public/js/vendor/vue/dist/vue.js",
                        "~/Public/common/myPlugin.js",
                        "~/Public/views/FormMaker/global.js",
                        "~/Public/views/FormMaker/FdItems.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Public/js/jquery/jquery.js",
                        "~/Public/common/myPlugin.js",
                        "~/Public/js/vendor/semantic-ui/dist/semantic.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Public/js/jquery/jquery-ui.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/site.css",
                        "~/Public/js/vendor/semantic-ui/dist/semantic.css",
                        "~/Content/m.css"
                        ));

            bundles.Add(new StyleBundle("~/Content/FormMaker").Include(
                    "~/Public/css/FormMaker/site.css",
                    "~/Public/js/vendor/semantic-ui/dist/semantic.css",
                    "~/Public/css/FormMaker/m.css"
            ));
        }
    }
}