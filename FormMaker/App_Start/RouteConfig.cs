﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FormMaker {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

           

            routes.MapRoute(
               name: "FormPreview",
               url: "Form/Preview/{FsID}",
               defaults: new { controller = "FormMaker", action = "PreviewTest", id = UrlParameter.Optional }
            );


            routes.MapRoute(
              name: "ResultStatic",
              url: "FormResult/{FsID}",
              defaults: new { controller = "FormMaker", action = "ResultStatic", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "FormMakker",
               url: "Form/{action}/{id}",
               defaults: new { controller = "FormMaker", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                  name: "FormMakkerAjax",
                  url: "Sync/{action}",
                  defaults: new { controller = "FormMakerAjax", action = "Index", id = UrlParameter.Optional }
              );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "FormMaker", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}