﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FormMaker.Models;
using FormMaker.ViewModels;
using FormMaker.ActionFilter;
using My.Attritube;

namespace FormMaker.Controllers
{
    public class FormMakerController : Controller
    {
        FormMakerEntities db;
        OLPEntities dbOLP;

        /// <summary>
        /// 從OLP轉過來的進入點!
        /// </summary>
        /// <param name="FsID"></param>
        /// <param name="Token"></param>
        /// <returns></returns>
        [AllowCrossSiteJsonAttribute]
        public ActionResult TransferToSurvey(string FsID, string Token)
        {
            using (dbOLP = new OLPEntities())
            {
                var es = dbOLP.UserRoles.Where(x => x.GID == Token);
                if (es.Count() == 0) { return RedirectToAction("TransferError"); }
                UserRole user = es.FirstOrDefault();
                Session["TSToken"] = user.GID;
                Session["UserID"] = user.Account;
                return RedirectToAction("PreviewTest", new { FsID=FsID  });
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="FsID"></param>
        /// <param name="Token">Token from dbo.FormMaker.CardiffMember.GID</param>
        /// <returns></returns>
        [AllowCrossSiteJsonAttribute]
        public ActionResult TransferFromCardiff(string FsID, string Token)
        {
            using (db = new FormMakerEntities())
            {
                var es = db.CardiffMembers.Where(x => x.GID == Token);
                if (es.Count() == 0) { return RedirectToAction("TransferError"); }

                CardiffMember user = es.FirstOrDefault();
                Session["TSToken"] = Token;
                Session["UserID"] = user.EmpNo;
                return RedirectToAction("PreviewTest", new { FsID = FsID });
            }
        }

        public ActionResult TransferError()
        {
            string OLPUrl = ConfigurationManager.AppSettings["OLP"].ToString();
            return Redirect(OLPUrl);
        }

        /// <summary>
        /// 總覽還沒開放...2014/12/12
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            string
                IsDev = ConfigurationManager.AppSettings["IsDev"].ToString(),
                UserID = "",
                UserName = "";

            if (IsDev == "Y")
            {
                UserID = ConfigurationManager.AppSettings["TesterID"].ToString();
                UserName = ConfigurationManager.AppSettings["Tester"].ToString();
                Session["UserID"] = UserID;
                Session["UserName"] = UserName;
            }
            ViewBag.Title = "個人表單總頁";
            ViewData["UserID"] = UserID;
            ViewData["UserName"] = UserName;
            using(db=new FormMakerEntities())
            {
                FormSetsViewModel vm = new FormSetsViewModel() {
                    FormSets = db.FormSets.Where(x=>x.IsEnable=="Y")
                            .OrderByDescending(x=>x.SN)
                            .ToList()
                    //.Where(x => x.Creator == UserID).ToList()
                };
                return View(vm);
            }
        }

        [HttpPost, CheckOneLabUserSession]
        public ActionResult SaveFormSet(FormSet fs) {
            using (db = new FormMakerEntities()) {
                if (fs.GID==null) {
                    fs.GID = System.Guid.NewGuid().ToString();
                    fs.Creator = ViewData["UserID"].ToString();
                    fs.CreateDate = DateTime.Now.ToString();
                    fs.IsEnable = "Y";
                    db.AddToFormSets(fs);
                } else {
                    FormSet e = db.FormSets.Where(x => x.GID == fs.GID).SingleOrDefault();
                    e.Dscr = fs.Dscr;
                    e.Name = fs.Name;
                    e.CreateDate = DateTime.Now.ToString();
                }
                db.SaveChanges();

            }
            return RedirectToAction("EditQuestion", new { d = fs.GID });
        }

        [CheckOneLabUserSession]
        public ActionResult NewForm() {
            string userid=ViewData["UserID"].ToString();
            string username = ViewData["UserName"].ToString();

            UserViewModel vm = new UserViewModel() {
                UserID = userid,
                UserName = username
            };
            return View(vm);
        }

        public ActionResult NewFormForAdmin()
        {
            //string userid = ViewData["UserID"].ToString();

            UserViewModel vm = new UserViewModel()
            {
                UserID = "ONE-0231",
                UserName = "Daniel.Chou"
            };
            return View("NewForm",vm);
        }

        [CheckOneLabUserSession, EffectivedDateForm(FormSetID="d")]
        public ActionResult EditQuestion(string d) {

            string GID = d;
            using (db = new FormMakerEntities())
            {
                var vm = db.FormSets.Where(x => x.GID == GID).SingleOrDefault();
                return View(vm);
            }
        }


        [CheckOneLabUserSession, EffectivedDateForm(FormSetID = "FsID")]
        public ActionResult PreviewTest(string FsID) {
            string GID = FsID;

            using (db = new FormMakerEntities())
            {
                var vm = db.FormSets.Where(x => x.GID == GID).SingleOrDefault();
                return View(vm);
            }
        }

        //[CheckOneLabUserSession]
        public ActionResult ResultStatic(string FsID)
        {
            string GID = FsID;
            using (db = new FormMakerEntities())
            {               
                FormSet m = db.FormSets.Where(x => x.GID == GID).SingleOrDefault();
                m.ISResultStatic = true;

                int TotalEmps = db.vEmployees.Count();
                int RealTested = db.vUserDatas.Where(x => x.FormID == GID).GroupBy(x=>x.UserID).Count();
                ViewData["TotalEmps"] = TotalEmps;
                ViewData["RealTested"] = RealTested;
                return View(m);
            }
        }

        [CheckOneLabUserSession]
        public ActionResult Finished(string FsID)
        {
            string GID = FsID;
            using (db = new FormMakerEntities())
            {
                var vm = db.FormSets.Where(x => x.GID == GID).SingleOrDefault();
                //ViewBag.UserID = Session["UserID"].ToString();
                //ViewBag.UserName = Session["UserName"].ToString();
                return View(vm);
            }
        }

        [HttpPost, CheckOneLabUserSession]
        public ActionResult CustomFormSave()
        {
            string FsID = Request["FsID"].ToString(),
                    ReqStr = "";

            string re_ip = "";
            if (Request.ServerVariables["HTTP_VIA"] != null)
                re_ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();  // Return real client IP.
            else
                re_ip = Request.ServerVariables["REMOTE_ADDR"].ToString(); //While it can't get the Client IP, it will return proxy IP.

            using (db = new FormMakerEntities()) {
                var qs = db.Fds.Where(x => x.FormID == FsID).ToList();
                int iRcc = Request.Form.Count;
                string UserID = ViewData["UserID"].ToString();
                string timestamp = DateTime.Now.ToShortDateString();

                //之前的紀錄全部刪除掉!!
                var vs = db.UserDatas.Where(x => x.UserID == UserID && x.FormID == FsID).ToList();
                foreach(var v in vs){
                    db.UserDatas.DeleteObject(v);
                }
                db.SaveChanges();

                var ff=db.UserDatas.Where(x => x.UserID == UserID && x.FormID == FsID && x.Note==null).ToList();
                foreach (var f in ff) {
                    f.Note = timestamp;
                }

                foreach (Fd q in qs)
                {
                    ReqStr = q.GID;
                    var v = Request[ReqStr];
                    //ss += "[" + ReqStr + "]:" + v + "@";
                    string[] varr = v.Split(new string[] { "," }, StringSplitOptions.None);
                    string QsTyp = varr[0];

                    if (QsTyp == "TextArea")
                    {
                        string TextAreaValue = v.Replace("TextArea,","");
                        UserData ud = new UserData()
                        {
                            GID = System.Guid.NewGuid().ToString().Replace("-", ""),
                            FormID = FsID,
                            QsTyp = QsTyp,
                            FdID = ReqStr,
                            ValueID = TextAreaValue,
                            UserID = UserID,
                            CreateDate = DateTime.Now,
                            IP = re_ip
                        };
                        db.AddToUserDatas(ud);
                    }
                    else
                    {
                        //每個選項都儲存起來！
                        for (int i = 1; i < varr.Length; i++)
                        {
                            UserData ud = new UserData()
                            {
                                GID = System.Guid.NewGuid().ToString().Replace("-", ""),
                                FormID = FsID,
                                QsTyp = QsTyp,
                                FdID = ReqStr,
                                ValueID = varr[i].ToString(),
                                UserID = UserID,
                                CreateDate = DateTime.Now,
                                IP = re_ip
                            };
                            db.AddToUserDatas(ud);
                        }
                    }
                    db.SaveChanges();
                }
                return RedirectToAction("Finished", new { FsID = FsID });
            }
            
        }

        /// <summary>
        /// 錯誤畫面!
        /// </summary>
        /// <returns></returns>
        public ActionResult OddPage() {
            ViewData["Msg"] = "該調查活動已結束，謝謝您的熱心參與。";
            return View();
        }

        /// <summary>
        /// 你已經調查過了，謝謝!
        /// </summary>
        /// <returns></returns>
        public ActionResult AreadyDone(UserData u) {
            return View(u);
        }

    }
}
