﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FormMaker.Models;
using FormMaker.ViewModels;
using FormMaker.ActionFilter;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FormMaker.Controllers
{
    public class FormMakerAjaxController : Controller
    {
        FormMakerEntities db;

        [HttpPost]
        public JsonResult Fds(string FsID)
        {
            //IEnumerable<FdOption> Options = db.vFdOptions.Where(x=>x.FormID==FsID).AsQueryable();
            
            using(db =new FormMakerEntities()){
                var Fds = db.Fds.Where(x => x.FormID == FsID).AsQueryable()
                   .Select( g => new { 
                            GID=g.GID,
                            HelpText=g.HelpText, 
                            g.Title, 
                            g.QsTyp,
                            FdOptions =db.FdOptions.Where(x=>x.FdID==g.GID)
                                            .Select(d=>new {
                                                d.SortNo,
                                                d.SN,
                                                d.Text,
                                                d.Value,
                                                d.StaticTag //群組統計用
                                            }).OrderBy(x=>x.SortNo),
                            g.SortNo,
                            g.GroupStatic,
                            g.Placeholder,
                            g.IsRequire, 
                            g.IsOneLine })
                .ToList()
                .OrderBy(x=>x.SortNo);
                
                return Json(Fds);
            }
        }

        [HttpPost]
        public JsonResult GetOneFds(string FdID) {
            using (db = new FormMakerEntities()) {
                var fd = db.Fds.Where(x => x.GID == FdID)
                .Select(g => new {
                    g.GID,
                    g.HelpText,
                    g.Title,
                    g.QsTyp,
                    g.SortNo,
                    g.SN,            //快速識別用!
                    g.Placeholder,
                    g.IsRequire,
                    g.IsOneLine
                })
                .SingleOrDefault();
                return Json(fd);
            }
        }

        [HttpPost, CheckOneLabUserSession]
        public string FdSave(Fd fd) {
            using (db = new FormMakerEntities()) {

                if (string.IsNullOrEmpty(fd.GID)) {
                    fd.SortNo = db.Fds.Where(x => x.FormID == fd.FormID).Count() + 1; //排序號碼預設最後一個!
                    fd.GID = System.Guid.NewGuid().ToString();
                    fd.CreateDate = DateTime.Now;
                    fd.CreateUser = Session["UserID"].ToString();   //從CheckOneLabUserSession 導入UserID
                    db.AddToFds(fd);
                } else {
                    Fd e = db.Fds.Where(x => x.GID == fd.GID).SingleOrDefault();
                    e.Title = fd.Title;
                    e.Dscr = fd.Dscr;
                    e.HelpText = fd.HelpText;
                    e.QsTyp = fd.QsTyp;
                    e.IsOneLine = fd.IsOneLine;
                    e.IsRequire = fd.IsRequire;
                    e.IsValidation = fd.IsValidation;
                    e.Placeholder = fd.Placeholder;
                    e.UpdateTime = DateTime.Now;
                }
                db.SaveChanges();
            }
            return fd.GID;
        }

        [HttpPost]
        public void FdDelete(string gid) {
            using (db = new FormMakerEntities()) {

                if (!string.IsNullOrEmpty(gid)) {
                    Fd e = db.Fds.Where(x => x.GID == gid).SingleOrDefault();
                    db.DeleteObject(e);
                } 
                db.SaveChanges();
            }
        }

        
        [HttpPost]
        public string FdOptionSave(string FdID, string GID, string Text) {

            if (GID == null && Text == "") return "";

            using (db = new FormMakerEntities()) {
                //判斷同一題不能有相同的選項文字
                FdOption fdo = db.FdOptions.Where(x => x.FdID == FdID && x.Text==Text).SingleOrDefault();
                if (fdo == null) {
                    string gid = System.Guid.NewGuid().ToString();
                    FdOption new_fdo = new FdOption()
                    {
                        FdID = FdID,
                        GID = gid,
                        Text = Text,
                        Value = gid.Substring(0, 8),
                        CreateDate = DateTime.Now
                    };
                    db.AddToFdOptions(new_fdo);
                    db.SaveChanges();
                    return gid;
                } else {
                    //var e = db.FdOptions.Where(x => x.GID == GID && x.Text == Text).SingleOrDefault();
                    fdo.Text = Text;
                    fdo.UpdateTime = DateTime.Now;
                    db.SaveChanges();
                    return GID;
                }
               
            }
        }

        [HttpPost]
        public JsonResult ShowFdOptions(string FsID) {
            using (db = new FormMakerEntities()) {
                var Fdos = db.vFdOptions.Where(x => x.FormID == FsID)
                .Select(g => new {
                    g.FdID,
                    g.GID,
                    g.Text,
                    g.Value,
                    g.SortNo
                })
                .OrderBy(x => x.SortNo)
                .ToList();
                return Json(Fdos);
            }
        }

        [HttpPost]
        public void RemoveOption(string OptionID) {
            using (db = new FormMakerEntities()) {
                if (OptionID != null)
                {
                    FdOption fdo = db.FdOptions.Where(x => x.GID == OptionID).SingleOrDefault();
                    db.DeleteObject(fdo);
                    db.SaveChanges();
                }
            }
        }

        [HttpPost]
        public void SaveSort(string SortList) {
            //拆解格式: gid:sortNumber;
            using (db = new FormMakerEntities()) {
                string[] brr={},
                        arr = SortList.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                string gid="";
                int sortNo=0;

                foreach (string s in arr)
                {
                    brr = s.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                    gid=brr[0];
                    sortNo=Int16.Parse(brr[1]);
                    Fd fd = db.Fds.Where(x => x.GID == gid).SingleOrDefault();
                    fd.SortNo = sortNo;
                    fd.UpdateTime = DateTime.Now;
                }
                db.SaveChanges();
            }
        }

        
        [HttpPost]
        public void SaveOptionsSort(string SortList)
        {
            //拆解格式: gid:sortNumber;
            using (db = new FormMakerEntities()) {
                string[] brr={},
                        arr = SortList.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                string gid="";
                int sortNo=0;

                foreach (string s in arr)
                {
                    brr = s.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                    gid=brr[0];
                    sortNo=Int16.Parse(brr[1]);
                    FdOption fdo = db.FdOptions.Where(x => x.GID == gid).SingleOrDefault();
                    fdo.SortNo = sortNo;
                    fdo.UpdateTime = DateTime.Now;
                }
                db.SaveChanges();
            }
        }

        [HttpPost, CheckOneLabUserSession, EffectivedDateForm(FormSetID = "FsID")]
        public JsonResult UserSelectedAnswer(string FsID) {
            //if (Session["UserID"] == null) return Json("未登入");
            string UserID=Session["UserID"].ToString();

            using (db = new FormMakerEntities()) {
                var es = db.vUserDatas
                    .Where(x => x.FormID == FsID && x.UserID == UserID && x.Note==null)
                    .Select(g => new { 
                        g.QsTyp,
                        g.FdID,
                        g.ValueID
                    }).ToList();
                return Json(es);
            }
        }

        //[HttpPost, CheckOneLabUserSession]
        [HttpPost]
        public JsonResult UserSelectedAnswerStatic(string FsID)
        {
            //if (Session["UserID"] == null) return Json("未登入");
            //string UserID = Session["UserID"].ToString();

            using (db = new FormMakerEntities())
            {
                var es = db.vUserDatas
                            .Where(x => x.FormID == FsID && x.Note == null && (x.QsTyp=="MultiChoice" || x.QsTyp=="Checkboxs"))
                            //.GroupBy(g => new { g.FdID, g.ValueID })  
                            /*不錯的範例: Groupby for multipule values.
                                {OptKey: {FdID: "33e82d00-9fba-4fcc-8a0a-76603373900f", ValueID: "23bdf827"}, CC: 4}
                            */
                            .GroupBy(g => g.ValueID)
                            .Select(c => new{
                                VID=c.Key,
                                CC = c.Count()
                            }).ToList();

                var txts=db.vUserDatas.Where(
                            x=>x.FormID==FsID 
                                && x.Note==null 
                                && (x.QsTyp=="TextArea" || x.QsTyp=="Text"))
                            .Select(g=>new { 
                                g.FdID,
                                dscr=g.ValueID
                            }).ToList();

                return Json(new
                {
                    multis = es,
                    txts = txts
                });
            }
        }


    }
}
