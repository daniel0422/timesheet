﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FormMaker.Models;
using System.Data.Objects.DataClasses;

namespace FormMaker.Models
{
    //這邊應該也沒用到!
    [MetadataType(typeof(FdMetadata))]
    public partial class Fd : EntityObject
    {
        /// <summary>
        /// 覆蓋原本屬性就要放在這!
        /// </summary>
        private class FdMetadata
        {
        }

        public IEnumerable<FdOption> FdOptions { get; set; }
        public IEnumerable<vFdOption> vFdOptions { get; set; }
    }

    public partial class FormSet : EntityObject
    {
        public bool ISResultStatic { get; set; }
    }

    //這個沒有用到!
    public class FdOpt
    {
        public string SN { get; set; }
        public string SortNo { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }

}