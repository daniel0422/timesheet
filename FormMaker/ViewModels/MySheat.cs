﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FormMaker.Models;

namespace FormMaker.ViewModels {

    public class MySheatViewModel 
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public int CurrMonth { get; set; }
        public IEnumerable<string> WkProdItems { get; set; }
    }

    //public class UserViewModel {
    //    public string UserID { get; set; }
    //    public string UserName { get; set; }
    //}
}