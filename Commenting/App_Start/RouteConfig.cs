﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Commenting
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Core Really QNA Display",
                url: "CMT/0/{keyCode}/{Token}",
                defaults: new { controller = "Core", action = "ShutJS", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Core stuff",
                url: "cmt/{action}",
                defaults: new { controller = "Core", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "RegisterPage",
                url: "RegisterKey/m/",
                defaults: new { controller = "Demo", action = "Register", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Demo", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}