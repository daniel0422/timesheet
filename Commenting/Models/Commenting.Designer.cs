﻿//------------------------------------------------------------------------------
// <auto-generated>
//    這個程式碼是由範本產生。
//
//    對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//    如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[assembly: EdmSchemaAttribute()]
namespace Commenting.Models
{
    #region 內容
    
    /// <summary>
    /// 沒有可用的中繼資料文件。
    /// </summary>
    public partial class CommentingEntities : ObjectContext
    {
        #region 建構函式
    
        /// <summary>
        /// 使用在應用程式組態檔的 'CommentingEntities' 區段中找到的連接字串，初始化新的 CommentingEntities 物件。
        /// </summary>
        public CommentingEntities() : base("name=CommentingEntities", "CommentingEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// 初始化新的 CommentingEntities 物件。
        /// </summary>
        public CommentingEntities(string connectionString) : base(connectionString, "CommentingEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// 初始化新的 CommentingEntities 物件。
        /// </summary>
        public CommentingEntities(EntityConnection connection) : base(connection, "CommentingEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        #endregion
    
        #region 部分方法
    
        partial void OnContextCreated();
    
        #endregion
    
        #region ObjectSet 屬性
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        public ObjectSet<Tag> Tags
        {
            get
            {
                if ((_Tags == null))
                {
                    _Tags = base.CreateObjectSet<Tag>("Tags");
                }
                return _Tags;
            }
        }
        private ObjectSet<Tag> _Tags;
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        public ObjectSet<TagedCmmt> TagedCmmts
        {
            get
            {
                if ((_TagedCmmts == null))
                {
                    _TagedCmmts = base.CreateObjectSet<TagedCmmt>("TagedCmmts");
                }
                return _TagedCmmts;
            }
        }
        private ObjectSet<TagedCmmt> _TagedCmmts;
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        public ObjectSet<Comment> Comments
        {
            get
            {
                if ((_Comments == null))
                {
                    _Comments = base.CreateObjectSet<Comment>("Comments");
                }
                return _Comments;
            }
        }
        private ObjectSet<Comment> _Comments;
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        public ObjectSet<Host> Hosts
        {
            get
            {
                if ((_Hosts == null))
                {
                    _Hosts = base.CreateObjectSet<Host>("Hosts");
                }
                return _Hosts;
            }
        }
        private ObjectSet<Host> _Hosts;

        #endregion

        #region AddTo 方法
    
        /// <summary>
        /// 將新物件加入 Tags EntitySet 的方法已被取代。請考慮改為使用關聯的 ObjectSet&lt;T&gt; 屬性的 .Add 方法。
        /// </summary>
        public void AddToTags(Tag tag)
        {
            base.AddObject("Tags", tag);
        }
    
        /// <summary>
        /// 將新物件加入 TagedCmmts EntitySet 的方法已被取代。請考慮改為使用關聯的 ObjectSet&lt;T&gt; 屬性的 .Add 方法。
        /// </summary>
        public void AddToTagedCmmts(TagedCmmt tagedCmmt)
        {
            base.AddObject("TagedCmmts", tagedCmmt);
        }
    
        /// <summary>
        /// 將新物件加入 Comments EntitySet 的方法已被取代。請考慮改為使用關聯的 ObjectSet&lt;T&gt; 屬性的 .Add 方法。
        /// </summary>
        public void AddToComments(Comment comment)
        {
            base.AddObject("Comments", comment);
        }
    
        /// <summary>
        /// 將新物件加入 Hosts EntitySet 的方法已被取代。請考慮改為使用關聯的 ObjectSet&lt;T&gt; 屬性的 .Add 方法。
        /// </summary>
        public void AddToHosts(Host host)
        {
            base.AddObject("Hosts", host);
        }

        #endregion

    }

    #endregion

    #region 實體
    
    /// <summary>
    /// 沒有可用的中繼資料文件。
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="CommentingModel", Name="Comment")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class Comment : EntityObject
    {
        #region Factory 方法
    
        /// <summary>
        /// 建立新 Comment 物件。
        /// </summary>
        /// <param name="gID">GID 屬性的初始值。</param>
        public static Comment CreateComment(global::System.String gID)
        {
            Comment comment = new Comment();
            comment.GID = gID;
            return comment;
        }

        #endregion

        #region 基本屬性
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String GID
        {
            get
            {
                return _GID;
            }
            set
            {
                if (_GID != value)
                {
                    OnGIDChanging(value);
                    ReportPropertyChanging("GID");
                    _GID = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("GID");
                    OnGIDChanged();
                }
            }
        }
        private global::System.String _GID;
        partial void OnGIDChanging(global::System.String value);
        partial void OnGIDChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String HostID
        {
            get
            {
                return _HostID;
            }
            set
            {
                OnHostIDChanging(value);
                ReportPropertyChanging("HostID");
                _HostID = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("HostID");
                OnHostIDChanged();
            }
        }
        private global::System.String _HostID;
        partial void OnHostIDChanging(global::System.String value);
        partial void OnHostIDChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String ParentID
        {
            get
            {
                return _ParentID;
            }
            set
            {
                OnParentIDChanging(value);
                ReportPropertyChanging("ParentID");
                _ParentID = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("ParentID");
                OnParentIDChanged();
            }
        }
        private global::System.String _ParentID;
        partial void OnParentIDChanging(global::System.String value);
        partial void OnParentIDChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String LoginName
        {
            get
            {
                return _LoginName;
            }
            set
            {
                OnLoginNameChanging(value);
                ReportPropertyChanging("LoginName");
                _LoginName = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("LoginName");
                OnLoginNameChanged();
            }
        }
        private global::System.String _LoginName;
        partial void OnLoginNameChanging(global::System.String value);
        partial void OnLoginNameChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String CallName
        {
            get
            {
                return _CallName;
            }
            set
            {
                OnCallNameChanging(value);
                ReportPropertyChanging("CallName");
                _CallName = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("CallName");
                OnCallNameChanged();
            }
        }
        private global::System.String _CallName;
        partial void OnCallNameChanging(global::System.String value);
        partial void OnCallNameChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String Comment1
        {
            get
            {
                return _Comment1;
            }
            set
            {
                OnComment1Changing(value);
                ReportPropertyChanging("Comment1");
                _Comment1 = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("Comment1");
                OnComment1Changed();
            }
        }
        private global::System.String _Comment1;
        partial void OnComment1Changing(global::System.String value);
        partial void OnComment1Changed();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Int32> UpVote
        {
            get
            {
                return _UpVote;
            }
            set
            {
                OnUpVoteChanging(value);
                ReportPropertyChanging("UpVote");
                _UpVote = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("UpVote");
                OnUpVoteChanged();
            }
        }
        private Nullable<global::System.Int32> _UpVote;
        partial void OnUpVoteChanging(Nullable<global::System.Int32> value);
        partial void OnUpVoteChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Int32> DownVote
        {
            get
            {
                return _DownVote;
            }
            set
            {
                OnDownVoteChanging(value);
                ReportPropertyChanging("DownVote");
                _DownVote = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("DownVote");
                OnDownVoteChanged();
            }
        }
        private Nullable<global::System.Int32> _DownVote;
        partial void OnDownVoteChanging(Nullable<global::System.Int32> value);
        partial void OnDownVoteChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Int32> Rating
        {
            get
            {
                return _Rating;
            }
            set
            {
                OnRatingChanging(value);
                ReportPropertyChanging("Rating");
                _Rating = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("Rating");
                OnRatingChanged();
            }
        }
        private Nullable<global::System.Int32> _Rating;
        partial void OnRatingChanging(Nullable<global::System.Int32> value);
        partial void OnRatingChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String IsRemoved
        {
            get
            {
                return _IsRemoved;
            }
            set
            {
                OnIsRemovedChanging(value);
                ReportPropertyChanging("IsRemoved");
                _IsRemoved = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("IsRemoved");
                OnIsRemovedChanged();
            }
        }
        private global::System.String _IsRemoved;
        partial void OnIsRemovedChanging(global::System.String value);
        partial void OnIsRemovedChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String IsEditable
        {
            get
            {
                return _IsEditable;
            }
            set
            {
                OnIsEditableChanging(value);
                ReportPropertyChanging("IsEditable");
                _IsEditable = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("IsEditable");
                OnIsEditableChanged();
            }
        }
        private global::System.String _IsEditable;
        partial void OnIsEditableChanging(global::System.String value);
        partial void OnIsEditableChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String IsPublic
        {
            get
            {
                return _IsPublic;
            }
            set
            {
                OnIsPublicChanging(value);
                ReportPropertyChanging("IsPublic");
                _IsPublic = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("IsPublic");
                OnIsPublicChanged();
            }
        }
        private global::System.String _IsPublic;
        partial void OnIsPublicChanging(global::System.String value);
        partial void OnIsPublicChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> CreateAt
        {
            get
            {
                return _CreateAt;
            }
            set
            {
                OnCreateAtChanging(value);
                ReportPropertyChanging("CreateAt");
                _CreateAt = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("CreateAt");
                OnCreateAtChanged();
            }
        }
        private Nullable<global::System.DateTime> _CreateAt;
        partial void OnCreateAtChanging(Nullable<global::System.DateTime> value);
        partial void OnCreateAtChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> UpdateTime
        {
            get
            {
                return _UpdateTime;
            }
            set
            {
                OnUpdateTimeChanging(value);
                ReportPropertyChanging("UpdateTime");
                _UpdateTime = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("UpdateTime");
                OnUpdateTimeChanged();
            }
        }
        private Nullable<global::System.DateTime> _UpdateTime;
        partial void OnUpdateTimeChanging(Nullable<global::System.DateTime> value);
        partial void OnUpdateTimeChanged();

        #endregion

    
    }
    
    /// <summary>
    /// 沒有可用的中繼資料文件。
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="CommentingModel", Name="Host")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class Host : EntityObject
    {
        #region Factory 方法
    
        /// <summary>
        /// 建立新 Host 物件。
        /// </summary>
        /// <param name="gID">GID 屬性的初始值。</param>
        public static Host CreateHost(global::System.String gID)
        {
            Host host = new Host();
            host.GID = gID;
            return host;
        }

        #endregion

        #region 基本屬性
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String GID
        {
            get
            {
                return _GID;
            }
            set
            {
                if (_GID != value)
                {
                    OnGIDChanging(value);
                    ReportPropertyChanging("GID");
                    _GID = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("GID");
                    OnGIDChanged();
                }
            }
        }
        private global::System.String _GID;
        partial void OnGIDChanging(global::System.String value);
        partial void OnGIDChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String Name
        {
            get
            {
                return _Name;
            }
            set
            {
                OnNameChanging(value);
                ReportPropertyChanging("Name");
                _Name = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("Name");
                OnNameChanged();
            }
        }
        private global::System.String _Name;
        partial void OnNameChanging(global::System.String value);
        partial void OnNameChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String Title
        {
            get
            {
                return _Title;
            }
            set
            {
                OnTitleChanging(value);
                ReportPropertyChanging("Title");
                _Title = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("Title");
                OnTitleChanged();
            }
        }
        private global::System.String _Title;
        partial void OnTitleChanging(global::System.String value);
        partial void OnTitleChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String Referer
        {
            get
            {
                return _Referer;
            }
            set
            {
                OnRefererChanging(value);
                ReportPropertyChanging("Referer");
                _Referer = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("Referer");
                OnRefererChanged();
            }
        }
        private global::System.String _Referer;
        partial void OnRefererChanging(global::System.String value);
        partial void OnRefererChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String Page
        {
            get
            {
                return _Page;
            }
            set
            {
                OnPageChanging(value);
                ReportPropertyChanging("Page");
                _Page = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("Page");
                OnPageChanged();
            }
        }
        private global::System.String _Page;
        partial void OnPageChanging(global::System.String value);
        partial void OnPageChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String KeyCode
        {
            get
            {
                return _KeyCode;
            }
            set
            {
                OnKeyCodeChanging(value);
                ReportPropertyChanging("KeyCode");
                _KeyCode = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("KeyCode");
                OnKeyCodeChanged();
            }
        }
        private global::System.String _KeyCode;
        partial void OnKeyCodeChanging(global::System.String value);
        partial void OnKeyCodeChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String Replyers
        {
            get
            {
                return _Replyers;
            }
            set
            {
                OnReplyersChanging(value);
                ReportPropertyChanging("Replyers");
                _Replyers = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("Replyers");
                OnReplyersChanged();
            }
        }
        private global::System.String _Replyers;
        partial void OnReplyersChanging(global::System.String value);
        partial void OnReplyersChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> CreateAt
        {
            get
            {
                return _CreateAt;
            }
            set
            {
                OnCreateAtChanging(value);
                ReportPropertyChanging("CreateAt");
                _CreateAt = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("CreateAt");
                OnCreateAtChanged();
            }
        }
        private Nullable<global::System.DateTime> _CreateAt;
        partial void OnCreateAtChanging(Nullable<global::System.DateTime> value);
        partial void OnCreateAtChanged();

        #endregion

    
    }
    
    /// <summary>
    /// 沒有可用的中繼資料文件。
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="CommentingModel", Name="Tag")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class Tag : EntityObject
    {
        #region Factory 方法
    
        /// <summary>
        /// 建立新 Tag 物件。
        /// </summary>
        /// <param name="gID">GID 屬性的初始值。</param>
        public static Tag CreateTag(global::System.String gID)
        {
            Tag tag = new Tag();
            tag.GID = gID;
            return tag;
        }

        #endregion

        #region 基本屬性
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String GID
        {
            get
            {
                return _GID;
            }
            set
            {
                if (_GID != value)
                {
                    OnGIDChanging(value);
                    ReportPropertyChanging("GID");
                    _GID = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("GID");
                    OnGIDChanged();
                }
            }
        }
        private global::System.String _GID;
        partial void OnGIDChanging(global::System.String value);
        partial void OnGIDChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String Name
        {
            get
            {
                return _Name;
            }
            set
            {
                OnNameChanging(value);
                ReportPropertyChanging("Name");
                _Name = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("Name");
                OnNameChanged();
            }
        }
        private global::System.String _Name;
        partial void OnNameChanging(global::System.String value);
        partial void OnNameChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> CreateAt
        {
            get
            {
                return _CreateAt;
            }
            set
            {
                OnCreateAtChanging(value);
                ReportPropertyChanging("CreateAt");
                _CreateAt = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("CreateAt");
                OnCreateAtChanged();
            }
        }
        private Nullable<global::System.DateTime> _CreateAt;
        partial void OnCreateAtChanging(Nullable<global::System.DateTime> value);
        partial void OnCreateAtChanged();

        #endregion

    
    }
    
    /// <summary>
    /// 沒有可用的中繼資料文件。
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="CommentingModel", Name="TagedCmmt")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class TagedCmmt : EntityObject
    {
        #region Factory 方法
    
        /// <summary>
        /// 建立新 TagedCmmt 物件。
        /// </summary>
        /// <param name="tagID">TagID 屬性的初始值。</param>
        /// <param name="cmmtID">CmmtID 屬性的初始值。</param>
        public static TagedCmmt CreateTagedCmmt(global::System.String tagID, global::System.String cmmtID)
        {
            TagedCmmt tagedCmmt = new TagedCmmt();
            tagedCmmt.TagID = tagID;
            tagedCmmt.CmmtID = cmmtID;
            return tagedCmmt;
        }

        #endregion

        #region 基本屬性
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String TagID
        {
            get
            {
                return _TagID;
            }
            set
            {
                if (_TagID != value)
                {
                    OnTagIDChanging(value);
                    ReportPropertyChanging("TagID");
                    _TagID = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("TagID");
                    OnTagIDChanged();
                }
            }
        }
        private global::System.String _TagID;
        partial void OnTagIDChanging(global::System.String value);
        partial void OnTagIDChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String CmmtID
        {
            get
            {
                return _CmmtID;
            }
            set
            {
                if (_CmmtID != value)
                {
                    OnCmmtIDChanging(value);
                    ReportPropertyChanging("CmmtID");
                    _CmmtID = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("CmmtID");
                    OnCmmtIDChanged();
                }
            }
        }
        private global::System.String _CmmtID;
        partial void OnCmmtIDChanging(global::System.String value);
        partial void OnCmmtIDChanged();
    
        /// <summary>
        /// 沒有可用的中繼資料文件。
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> CreateAt
        {
            get
            {
                return _CreateAt;
            }
            set
            {
                OnCreateAtChanging(value);
                ReportPropertyChanging("CreateAt");
                _CreateAt = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("CreateAt");
                OnCreateAtChanged();
            }
        }
        private Nullable<global::System.DateTime> _CreateAt;
        partial void OnCreateAtChanging(Nullable<global::System.DateTime> value);
        partial void OnCreateAtChanged();

        #endregion

    
    }

    #endregion

    
}
