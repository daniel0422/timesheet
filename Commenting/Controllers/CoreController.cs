﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Commenting.Models;
using Commenting.ActionFilter;
using My.Attritube;
using System.Net;
using System.Configuration;
using System.IO;
using Commenting.ViewModel;


namespace Commenting.Controllers
{
    public class CoreController : Controller
    {
        CommentingEntities db;
        OLPEntities dbOLP;
        HRMISEntities HRMISdb;

        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// 透過Portal提供每個人差異化的網址進來，若是透過JS Client cookie認證就不會從這邊進入
        /// </summary>
        /// <param name="AppID"></param>
        /// <param name="Token"></param>
        /// <returns></returns>
        [AllowCrossSiteJsonAttribute]
        public ActionResult Transfer2QNA(string AppID, string Token)
        {
            using (dbOLP = new OLPEntities())
            {
                var es = dbOLP.UserRoles.Where(x => x.GID == Token);
                if (es.Count() == 0) { return RedirectToAction("TransferError"); }
                
                UserRole user = es.FirstOrDefault();
                string UserID = user.Account;
                string UserName = "";
                Session["UserToken"] = Token;
                Session["UserID"] = UserID;
                var aes = dbOLP.Accounts.Where(x => x.EmployeeID == UserID).ToList();
                if (aes.Count() > 0)
                {
                    UserName = aes.SingleOrDefault().username;
                    Session["UserName"] = UserName;
                }

                return RedirectToAction("Index", "Demo", new { AppID = AppID });
            }
        }

        public ActionResult TransferError()
        {
            string OLPUrl = ConfigurationManager.AppSettings["OLP"].ToString();
            return Redirect(OLPUrl);
        }

        /// <summary>
        /// 這邊會檢核HostServer檢查Key是否有對應，沒有被人家隨便亂用!
        /// </summary>
        /// <param name="gid"></param>
        /// <returns></returns>
        [GetCmmtGUID(KeyCode = "KeyCode", GIDX = "Gid")]
        public JavaScriptResult ShutJS(string KeyCode, string GIDX, string Token)
        {
            string OLPUrl = ConfigurationManager.AppSettings["OLP"].ToString();
            //從~/Public/js/Commenting.js 中抓取JS碼再拿進來加工
            //可將使用者資訊自動注入JS當中....
            //JsSource = "$('CommentingContent').append('<div>KeyCode:{0}，對應到GID:{1}</div>');",
            //JsSource = String.Format(JsSource, KeyCode, GIDX),    

            string Referer = Request.Headers["Referer"].ToLower().ToString(),
                   HostUrl = ConfigurationManager.AppSettings["WebHost"].ToString(),
                   TestUser = ConfigurationManager.AppSettings["TestUser"].ToString(),
                   ReplyerStr = "",
                   CmtTitle = "",
                   jsCode = System.IO.File.ReadAllText(Server.MapPath(@"~/Public/js/Commenting.js")),
                   IsReplyer ="false",  //Default as "false"
                   TimeStamp = DateTime.Now.Ticks.ToString(),
                   UserToken = "",
                   UserName = "",
                   UserID = "";

            //這邊是測試使用!!否則就從Portal導入進去
            if (TestUser != "")
            {
                using (dbOLP = new OLPEntities())
                {
                    var e = dbOLP.Accounts.Where(x => x.EmployeeID == TestUser).SingleOrDefault();
                    UserID = TestUser;
                    UserName = e.username;
                    var r = dbOLP.UserRoles.Where(x => x.Account == TestUser).FirstOrDefault();
                    UserToken = r.GID;
                }
            }
            else{
                //UserToken from Links....
                if (Token != "")
                {
                    using (dbOLP = new OLPEntities())
                    {
                        UserToken = Token;
                        var r = dbOLP.UserRoles.Where(x => x.GID == Token).FirstOrDefault();
                        UserID = r.Account;
                        var e = dbOLP.Accounts.Where(x => x.EmployeeID == UserID).SingleOrDefault();
                        UserName = e.username;
                    }
                }
                else
                {
                    if (Session["UserToken"] != null)
                    {
                        UserToken = Session["UserToken"].ToString();
                        UserName = Session["UserName"].ToString();
                        UserID = Session["UserID"].ToString();
                    }
                    else {
                        return JavaScript("location.href='" + OLPUrl + "'");
                    }
                }
            }

            using (db = new CommentingEntities()) {
                var e =db.Hosts.Where(x => x.KeyCode == KeyCode).SingleOrDefault();
                ReplyerStr = e.Replyers;
                CmtTitle = e.Title;
                IsReplyer = (ReplyerStr.IndexOf(UserID) >= 0) ? "true" : "false";
            }

            return JavaScript(
                    jsCode.Replace("@UserToken@", UserToken)
                          .Replace("@HostUrl@",HostUrl)
                          .Replace("@UserName@", UserName)
                          .Replace("@UserID@", UserID)
                          .Replace("@IsReplyer@", IsReplyer)
                          .Replace("@timestamp@", TimeStamp)
                          .Replace("@CmtTitle@", CmtTitle)
                );
        }

        [HttpPost]
        [AllowCrossSiteJsonAttribute]
        [GetUserIDByToken(UserToken = "UserToken", UserID = "UserID", UserName = "UserName")]
        [GetCmmtGUID(KeyCode = "KeyCode", GIDX = "Gid")]
        public void SaveCmmt(string KeyCode, string GIDX
                            , string UserToken, string UserID, string UserName
                            , string qs, string pid, string isPublic, string gid)
        {
            if (gid == null)
            {
                string Gid = System.Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
                using (db = new CommentingEntities())
                {
                    Comment cmt = new Comment()
                    {
                        CallName = UserName,
                        LoginName = UserID,
                        ParentID = pid,
                        Comment1 = qs,
                        IsPublic = isPublic,
                        CreateAt = DateTime.Now,
                        GID = Gid,
                        HostID = GIDX
                    };
                    db.AddToComments(cmt);
                    db.SaveChanges();
                }
                SendEmail(KeyCode, Gid, "AddNew");
            }
            else {
                using (db = new CommentingEntities())
                {
                    var e = db.Comments.Where(x => x.GID == gid).SingleOrDefault();
                    e.Comment1 = qs;
                    e.IsPublic = isPublic;
                    e.UpdateTime = DateTime.Now;
                    db.SaveChanges();
                }
                SendEmail(KeyCode, gid, "Update");
            }
        }



        [HttpPost]
        [AllowCrossSiteJsonAttribute]
        [GetUserIDByToken(UserToken = "UserToken", UserID = "UserID", UserName = "UserName")]
        [GetCmmtGUID(KeyCode = "KeyCode", GIDX = "Gid")]
        public void ReplyCmmt(string KeyCode, string GIDX
                            , string UserToken, string UserID, string UserName
                            , string qs, string pid, string isPublic, string gid)
        {

            string Gid = System.Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
            using (db = new CommentingEntities())
            {
                Comment cmt = new Comment()
                {
                    CallName = UserName,
                    LoginName = UserID,
                    ParentID = gid,
                    GID = Gid,
                    Comment1 = qs,
                    IsPublic = isPublic,
                    CreateAt = DateTime.Now,
                    HostID = GIDX
                };
                db.AddToComments(cmt);
                db.SaveChanges();
            }
            SendEmail(KeyCode, Gid, "Reply");

        }



        [HttpPost]
        [AllowCrossSiteJsonAttribute]
        [GetUserIDByToken(UserToken = "UserToken", UserID = "UserID", UserName = "UserName")]
        [GetCmmtGUID(KeyCode = "KeyCode", GIDX = "Gid")]
        public void RemoveSelfCmmt(string KeyCode, string GIDX
                            , string UserToken, string UserID, string UserName
                            , string gid)
        {
            bool IsAuth = false;
            using (db = new CommentingEntities())
            {
                var e = db.Comments.Where(x => x.GID == gid).SingleOrDefault();
                if (e.LoginName == UserID)
                {
                    IsAuth = true;
                    e.UpdateTime = DateTime.Now;
                    e.IsRemoved = "Y";
                    db.SaveChanges();
                }
                else { 
                    //TODO:請提供訊息回報...
                }
            }
            if (IsAuth)
            {
                SendEmail(KeyCode, gid, "Update");
            }
        }


        [HttpPost]
        [AllowCrossSiteJsonAttribute]
        [GetUserIDByToken(UserToken = "UserToken", UserID = "UserID", UserName = "UserName")]
        [GetCmmtGUID(KeyCode = "KeyCode", GIDX = "Gid")]
        public void RatingSave(string KeyCode, string GIDX
                            , string UserToken, string UserID, string UserName
                            , string gid , int RateNumber)
        {
            using (db = new CommentingEntities())
            {
                var e = db.Comments.Where(x => x.GID == gid).SingleOrDefault();
                e.UpdateTime = DateTime.Now;
                e.Rating = RateNumber;
                db.SaveChanges();
            }
        }


        //[CheckOneLabUserSession]
        [AllowCrossSiteJsonAttribute]
        [GetUserIDByToken(UserToken = "UserToken", UserID = "UserID", UserName = "UserName")]
        [GetCmmtGUID(KeyCode = "KeyCode", GIDX = "Gid")]
        public JsonDateResult ShowCmtList(string KeyCode, string GIDX
            , string UserToken, string UserID, string UserName)
        {
            string Replyers="";
            using (db = new CommentingEntities()){
                Replyers=db.Hosts.Where(x=>x.KeyCode== KeyCode).SingleOrDefault().Replyers;
            }
            using (db = new CommentingEntities())
            {
                var es = db.Comments.Where(x => x.HostID == GIDX && x.IsRemoved == null && x.ParentID == "").AsQueryable();
                //若是自己要回復的部門，全部都看得到....
                if(Replyers.IndexOf(UserID)<0){
                    es=es.Where(x=>x.LoginName == UserID || x.IsPublic=="Y").AsQueryable();
                }
                var ess=es.Select(g => new
                    {
                        gid = g.GID,
                        uid = g.LoginName,
                        cmmt = g.Comment1.Replace("\n","<br />"),
                        crAt = g.CreateAt,
                        cname = g.CallName,
                        pid = g.ParentID,
                        replys = db.Comments.Where(x=>x.ParentID == g.GID && x.ParentID!="")
                                            .Select( d=> new {
                                                gid=d.GID,
                                                uid=d.LoginName,
                                                cmmt = d.Comment1.Replace("\n", "<br />"),
                                                cname=d.CallName,
                                                crAt=d.CreateAt,
                                                rate=d.Rating })
                                            .OrderByDescending(x=>x.crAt),
                        ispub = g.IsPublic
                    })
                    .OrderByDescending(x => x.crAt).ToList();
                return new JsonDateResult() { Data = ess };
            }
        }

        //這樣是失敗的
        [HttpPost]
        public void GetRemotePortalToken(string oo) {
            string url = "http://localhost:42876/OnelabLoginPortal/GetAuth.ashx";
            WebClient wc = new WebClient();
            string data = wc.DownloadString(url);
        }

        [HttpPost]
        public void GetToken() {
            string t = MyPage.Token;
            string username = MyPage.UserName;
            
        }

        public class MyPage : SSOLib.PrivatePage
        {
            public static string Token { get; set; }
            public static string UserName { get; set; }

            protected void Page_Load(object sender, EventArgs e)
            {
                Token = CurrentUser.Token;
                UserName = CurrentUser.UserName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="KeyCode"></param>
        /// <param name="GID"></param>
        /// <param name="Status">AddNew:剛剛問|Update:異動了|Remove:移除了</param>
        public void SendEmail(string KeyCode, string GID, string Status)
        {
            EmailContent ec = new EmailContent();

            string  IsEnableSendMail = ConfigurationManager.AppSettings["IsEnableSendMail"].ToString(),
                    IsEnableTestEmail = ConfigurationManager.AppSettings["IsEnableTestEmail"].ToString(),
                    MailToTester = ConfigurationManager.AppSettings["MailToTester"].ToString(),
                    path = Server.MapPath("~/Views/Shared/FinQATemplate.htm"),
                    html = System.IO.File.ReadAllText(path),
                    title = html.Substring(html.IndexOf("<title>") + 7, html.IndexOf("</title>") - html.IndexOf("<title>") - 7),
                    subject = "",
                    body = "",

                    CommentingTitle = "",
                    Replyers = "",
                    UserName = "",
                    AskDateTime = "",
                    WhoAsk = "",
                    WhoAskID="",
                    WhoReplyed="",
                    StatusMsg = "",
                    Comment = "",
                    CommentPartial = "";

            if (IsEnableSendMail == "Y")
            {
                switch(Status){
                    case "AddNew": StatusMsg = "剛剛問"; break;
                    case "Update": StatusMsg = "異動了"; break;
                    case "Remove": StatusMsg = "移除了"; break;
                    case "Reply": StatusMsg = "回覆了"; break;
                }

                try
                {
                    using (db = new CommentingEntities())
                    {
                        var h = db.Hosts.Where(x => x.KeyCode == KeyCode).SingleOrDefault();
                        CommentingTitle = h.Title.Replace("<br />", "");
                        Replyers = h.Replyers;
                    }


                    using (db = new CommentingEntities())
                    {
                        var e = db.Comments.Where(x => x.GID == GID).SingleOrDefault();
                        AskDateTime = e.CreateAt.ToString();
                        WhoAsk = e.CallName;
                        Comment = e.Comment1.Replace("\n", "<br />");
                        CommentPartial = (e.Comment1.Length >= 20)
                          ? e.Comment1.Substring(0, 20) + "..."
                          : Comment;
                    }
                    
                    if (Status == "Reply")
                    {
                        path = Server.MapPath("~/Views/Shared/FinQAReplyTemplate.htm");
                        html = System.IO.File.ReadAllText(path);
                        AskDateTime = "";
                        Comment="";

                        using (db = new CommentingEntities())
                        {
                            var f = db.Comments.Where(x => x.GID == GID).SingleOrDefault();
                            string parentid = f.ParentID;
                                WhoReplyed = f.CallName;
                            var origin = db.Comments.Where(x=>x.GID==parentid).SingleOrDefault();
                                WhoAskID = origin.LoginName;
                            var es = db.Comments.Where(x => x.ParentID == parentid || x.GID == parentid).OrderBy(x => x.CreateAt).ToList();
                            foreach (var e in es)
                            {
                                Comment += e.CallName+"&nbsp;"+e.CreateAt+"<br />"+e.Comment1.Replace("\n", "<br />")+"<hr /><br />";
                            }
                        }
                    }

                    subject = title.Replace("{{CommentingTitle}}", CommentingTitle)
                                   .Replace("{{WhoAsk}}", WhoAsk)
                                   .Replace("{{WhoReplyed}}", WhoReplyed)
                                   .Replace("{{Status}}", StatusMsg)
                                   .Replace("{{CommentPartial}}", CommentPartial);

                    using (dbOLP = new OLPEntities())
                    {
                        if (Status == "Reply") { Replyers= Replyers + "," + WhoAskID; }
                        string[] ReplyArr = Replyers.Split(new string[] { "," }, StringSplitOptions.None);
                        
                        foreach (string reply in ReplyArr)
                        {
                            var user = dbOLP.Accounts.Where(x => x.EmployeeID == reply).SingleOrDefault();
                            if (user != null)
                            {
                                UserName = user.username;
                                body = html.Replace("{{UserName}}", UserName)
                                            .Replace("{{WhoAsk}}", WhoAsk)
                                            .Replace("{{AskDateTime}}", AskDateTime)
                                            .Replace("{{Comment}}", Comment);

                                using (HRMISdb = new HRMISEntities())
                                {
                                    A_MailList mail = new A_MailList()
                                    {
                                        Body = body,
                                        FromDB = "Commenting",
                                        Subject = subject,
                                        CreateTime = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"),
                                        MailFrom = "onelab.mis@onelab.tw",
                                        MailTo = (IsEnableTestEmail == "Y") ? MailToTester : user.email,
                                        mGUID = System.Guid.NewGuid().ToString().Replace("-", "")
                                    };
                                    HRMISdb.AddToA_MailList(mail);
                                    HRMISdb.SaveChanges();
                                }
                            }
                        }
                      

                    }

                }
                catch (Exception ex)
                {
                    //TODOs:

                }
            }
        }

    }
}
