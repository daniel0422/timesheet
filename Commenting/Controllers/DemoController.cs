﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Commenting.Controllers
{
    public class DemoController : Controller
    {
        //
        // GET: /Demo/

        public ActionResult Index()
        {
           
            return View();
        }

        public ActionResult TestMe() {
            return View();
        }

        //Redirct to register page while someone provide the wrong keycode in js sippet.
        public ActionResult Register() 
        {
            return View();
        }

    }
}
