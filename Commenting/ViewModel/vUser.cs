﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Commenting.ViewModel
{
    public class vUser
    {
        public string ID { get; set; }
        public string FullName { get; set; }
        public string Token { get; set; }
    }

    public class EmailContent
    {
        public string totalMember { get; set; }
        public string body { get; set; }
        public string ErrMsg { get; set; }
    }
}