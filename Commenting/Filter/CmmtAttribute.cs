﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Commenting.Models;
using System.Web.Routing;

namespace Commenting.ActionFilter
{
    /// <summary>
    /// 檢查OneLab員工是否有Session[UserID]存在，若不存在導入到Portal去登入。
    /// </summary>
    public class CheckOneLabUserSessionAttribute : ActionFilterAttribute
    {
       

        public override void OnActionExecuting(ActionExecutingContext filterContext) {

            HttpContextBase hc = filterContext.HttpContext;
            string OLP_Url = ConfigurationManager.AppSettings["OLP"].ToString();

            if (hc.Session["UserID"] == null 
                 //|| hc.Session["TSToken"]== null
                )
            {
                filterContext.Result = new RedirectResult(OLP_Url);
            }
            else
            {

                filterContext.Controller.ViewData["UserID"] = hc.Session["UserID"].ToString();
            }
        }
    }

    public class GetUserIDByToken : ActionFilterAttribute {
        public string UserToken { get; set; }   //input
        public string UserID { get; set; }  //output
        public string UserName { get; set; }    //out

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            OLPEntities dbOLP;
            ActionExecutingContext fc = filterContext;
            string myToken;
            if (fc.ActionParameters.ContainsKey("UserToken"))
            {
                myToken = fc.ActionParameters["UserToken"].ToString();
                using (dbOLP = new OLPEntities()) {
                    var es = dbOLP.UserRoles.Where(x => x.GID == myToken).ToList();
                    if (es.Count()>0) {
                        var e = es.First();
                        UserID = e.Account;
                        fc.ActionParameters["UserID"] = UserID;
                        //Get UserName from other Account Table.
                        var aes = dbOLP.Accounts.Where(x => x.EmployeeID == UserID).ToList();
                        if (aes.Count() > 0) 
                        {
                            UserName = aes.SingleOrDefault().username;
                            fc.ActionParameters["UserName"] = UserName;
                        }
                        
                    }

                }
            } else { 
                fc.Result=new RedirectToRouteResult(
                    new RouteValueDictionary{ {"controller", "Core"}, {"action","TransferError"} }
                );
            }
        }
    }


    public class GetCmmtGUIDAttribute : ActionFilterAttribute
    {
        public string KeyCode { get; set; }
        public string GIDX { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CommentingEntities db;
            ActionExecutingContext fc = filterContext;
            string KeyCode = "",
                    //JsSource = "$('CommentingContent').append('<div>KeyCode:{0}，對應到GID:{1}</div>');",
                    HostUrl=System.Configuration.ConfigurationManager.AppSettings["WebHost"].ToString();

            if (fc.ActionParameters.ContainsKey("KeyCode"))
            {
                KeyCode = fc.ActionParameters["KeyCode"].ToString();
            }
            else
            {
                fc.Result = new RedirectToRouteResult(
                        new RouteValueDictionary{  {"controller", "Demo"}, {"action", "RegisterKey"}
                    });
            }

            using (db = new CommentingEntities())
            {
                var es = db.Hosts.Where(x => x.KeyCode == KeyCode).ToList();
                string gid = "";

                if (es.Count > 0)
                {
                    gid = es.SingleOrDefault().GID;
                    GIDX = gid;
                    fc.ActionParameters["GIDX"] = GIDX;
                }
                else
                {
                    //return JavaScript("location.href='" + HostUrl + "/RegisterKey/m/'");
                    fc.Result = new RedirectToRouteResult(
                        new RouteValueDictionary{
                            {"controller", "Demo"},
                            {"action", "Register"}
                        });
                }
            }

        }


    }


}