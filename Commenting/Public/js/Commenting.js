﻿String.format = function () {
    var s = arguments[0];
    if (s == null) return "";
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = getStringFormatPlaceHolderRegEx(i);
        s = s.replace(reg, (arguments[i + 1] == null ? "" : arguments[i + 1]));
    }
    return cleanStringFormatResult(s);
}

String.prototype.format = function () {
    var txt = this.toString();
    for (var i = 0; i < arguments.length; i++) {
        var exp = getStringFormatPlaceHolderRegEx(i);
        txt = txt.replace(exp, (arguments[i] == null ? "" : arguments[i]));
    }
    return cleanStringFormatResult(txt);
}

function getStringFormatPlaceHolderRegEx(placeHolderIndex) {
    return new RegExp('({)?\\{' + placeHolderIndex + '\\}(?!})', 'gm')
}

function cleanStringFormatResult(txt) {
    if (txt == null) return ""; return txt.replace(getStringFormatPlaceHolderRegEx("\\d+"), "");
}

/*-------------------------------------------------------------------------*/
Cmmt = (function () {
    
    var KeyCode22 = "@KeyCode@",
        host = "@HostUrl@",
        gb_MainContainer = "<div class='ccmain'><h4 id='qa-title'>@CmtTitle@</h4><div class='cmtHistory''>Loading...</div></div>",
        gb_cmmtTemp = "<div class='cmmt' gid='{0}' pid='{1}'><div class='callName' data-uid='{5}'>{2}</div><div class='btns'><div class='ui button cmt-reply' onclick='Cmmt.Reply(this)'><i class='reply icon'></i>Reply</div><div class='ui button cmt-remove' onclick='Cmmt.Remove(this)'><i class='remove icon'></i>Remove</div></div><div class='createAt'>{4}</div>{7}<div class='cmmtBody editable'><span  class='cmt-block' title='Click to edit it.'>{3}</span>{6}</div></div>",
        gb_replyedTemp = "<div class='replyed' gid='{0}' pid='{1}'><i class='level up icon left'></i><div class='callName' data-uid='{5}'>{2}</div><div class='createAt'>{4}</div><div class='ratebox'><div class='ui star rating' rates='{6}' data-rating='{6}' data-max-rating='10'></div><RateDscr>Please rate it.<i class='ui icon pointing right'></i></RateDscr></div><div class='cmmtBody'><span class='{7}'>{3}</span></div></div>",
        gb_edit = "<editpanel><table id='cmt-edit-table'><tr><td class='cmt-table-td-avator'><div class='avator'><div class='avator-inner'></div></div></td><td><textarea class='provide-qs' placeholder='以 @UserName@ 的身分提問討論...'></textarea></td></tr></table><div class='cmtbtns'><button class='btn-ask-submit' onclick='Cmmt.Save()'>Submit</button><label class='lb-ispublic'><input type='checkbox' id='isPublic' />公開這個討論，大家都看得到</label></div></editpanel>",
        avator_image='http://192.168.6.20:85/Contact/getOnePhoto.aspx?empno=@UserID@&r=@timestamp@',
        UserToken = "@UserToken@",
        UserName = "@UserName@",
        UserID="@UserID@",
        IsReplyer = @IsReplyer@,
        Gid="",
        SaveType ="", /*ask|reply*/
        KeyCode = "31DE9D5A77424579BBA7CFDD5CCBD10B";

    var init = function () {
        $("head").append("<link rel='stylesheet' type='text/css' href='"+host+"/public/css/cmmt.css' />");
        var m = $("CommentingContent"),
                ck = "";

        m.append(gb_MainContainer);
        if(!IsReplyer) { CreateEditPanel( $("#qa-title"),"ask"); }

        this.ShowCmmtHistory();
        ck = getTokenByCookie();
    };

    var ShowCmmtHistory = function () {
        $.post(host + "/cmt/ShowCmtList", { KeyCode: KeyCode , UserToken: UserToken}, function (rs) {
            var ss = "", c = {}, r={}, arr=[], sss="", str_ispub="";
            for (var d in rs) {
                c = rs[d]; //console.log(c);
                sss="";
                if(c.replys.length>0){
                    arr=c.replys;
                    for (var e in arr)
                    {
                        r=arr[e];
                        /*console.log(r.cname, r.rate, UserName);*/
                        sss += String.format(gb_replyedTemp, r.gid, r.pid, r.cname, r.cmmt, r.crAt , r.uid, r.rate,  (UserName===r.cname)?"replyEdit":"");
                    }
                }
                str_ispub=(c.ispub==="Y")?"":"<i class='unhide blue icon ispublic-icon' title='This is a private question.'></i>";
                ss += String.format(gb_cmmtTemp, c.gid, c.pid, c.cname, c.cmmt, c.crAt , c.uid, sss , str_ispub); //moment([2015,1,3]).fromNow());
            }
            $(".cmtHistory").html(ss);
            if(!IsReplyer) { $(".btns .cmt-reply").remove(); }
            $(".cmmt").each(function(i,n){
                var $me=$(n)
                    , uid=$me.find(".callName").attr("data-uid")
                    , cc=$me.find(".replyed").size()
                    , ccRates = $me.find(".star.rating");

                //console.log(uid, "@UserID@", IsReplyer);
                if(uid != "@UserID@" || cc>0) {
                    $me.find(".cmt-remove").remove();
                    $me.find(".cmmtBody").removeClass("editable").removeAttr("title");
                }
                
                if(uid !="@UserID@" && !IsReplyer) {
                    $me.find(".ratebox").remove();

                }
                else
                {
                    /*計算該出現多少星星?*/
                    ccRates.each(function(i,n){
                        var $starBox=$(n),
                            m=$(n).closest(".replyed").attr("gid"),
                            rating=$(n).attr("rates");
                        
                        
                        if(rating != "") { 
                            //console.log('已經打過分數!', m, rating); 
                            $starBox.parent().find("ratedscr").remove(); 
                            $(n).rating("disable");
                        }else{
                            if(IsReplyer) { $starBox.parent().remove(); }/*Rating整個移除掉*/     
                        }
                        //if(rating == "" && IsReplyer) { $starBox.parent().remove(); }/*Rating整個移除掉*/ 
                    });
                }
            });

            //$('.ui.rating').rating({ rating: 5, maxRating: 10 });
            $('.ui.rating').rating('setting', 'onRate', function(value) {
                var gid=$(this).closest(".replyed").attr("gid");
                //console.log(gid, value);
                $.post(host+"/cmt/RatingSave", { KeyCode: KeyCode, UserToken: UserToken, gid:gid, RateNumber:value }, function(){
                    ShowCmmtHistory();
                });
            });
            $(".cmmtBody.editable span, .replyEdit").on("click",function(){
                var me=$(this).closest(".cmmt"),
                    gid=me.attr("gid"),
                    cmtSpan=me.find("span"),
                    regex=/<br>/g,
                    isPublic=me.find(".ispublic-icon").size(),
                    isPublicStr=(isPublic===1)?"":"checked",
                    cmtString=cmtSpan.html();
                
                /*console.log(gid, isPublic, isPublicStr);*/
                $(".cmt-block").show();
                CreateEditPanel( me.find("span"),"edit");
                cmtSpan.hide();
                if(isPublic===1){
                    $("#isPublic").removeAttr("checked"); }
                else{ 
                    $("#isPublic").attr("checked","checked");
                }
                $(".provide-qs").val(cmtString.replace(regex,"\n"));
            });

        });

       
    };

    var getTokenByCookie = function () {
        var ckstr = document.cookie, arr = ckstr.split(";"), ck = "", d = "";
        for (var c in arr) {
            d = arr[c];
            if (d && d.indexOf("AUTH_COOKIE") >= 0) {
                var ckarr = d.split("=");
                ck = ckarr[1];
                ckarr = ck.split("|");
                ck = ckarr[0];
            }
        }
        return ck;
    };

    var CreateEditPanel = function(e, mode) {
        $("editpanel").remove();
        $(e).after(gb_edit);
        $("editpanel").fadeIn(500);
        SaveType=mode;

        var ph="";
        if(mode==="reply"){
            ph=$(".provide-qs").attr("placeholder").replace("提問討論...","回應問題...");
            $(".provide-qs").attr("placeholder",ph);
            $(".lb-ispublic").remove();
            $(".btn-ask-submit").before("<button class='btn-cmt-cancel' onclick='Cmmt.CancelReply(this)'>Cancel</button>");
        }else if(mode==="edit"){
            $(".btn-ask-submit").before("<button class='btn-cmt-cancel' onclick='Cmmt.Cancel(this)'>Cancel</button>");
        }
        $(".provide-qs").fadeIn(400);
        $(".avator-inner").css("background-image" , 'url('+avator_image+')');
    }

    var Save = function () {
        var qs = $(".provide-qs").val();
        pid = (SaveType=="reply")? Gid:"";
        ispublic = ($("#isPublic").is(":checked"))?"Y":"";
        gid = $(".provide-qs").closest(".cmmt").attr("gid");
        console.log(SaveType, ispublic, gid, qs);
        var url=(SaveType==="reply")?"/cmt/ReplyCmmt":"/cmt/SaveCmmt";
        $.post(host + url, { KeyCode: KeyCode, UserToken: UserToken, qs: qs, pid:pid, isPublic: ispublic, gid: gid }, function () {
            ShowCmmtHistory();
        });

    };

    var Cancel = function(e){
        $(e).closest(".cmmtBody").find("span").show();
        CreateEditPanel( $("#qa-title"),"ask");
    }

    var Remove = function(e){
        var gid=$(e).closest(".cmmt").attr("gid");
        $.post(host+"/cmt/RemoveSelfCmmt", { KeyCode: KeyCode, UserToken: UserToken, gid:gid}, function(){
            ShowCmmtHistory();
        });
    }

    var Reply = function(e){
        Gid=$(e).closest(".cmmt").attr("gid"); 
        //console.log(Gid);
        CreateEditPanel($(e).closest(".cmmt").find(".cmt-block") , "reply");
    }

    var ReplySave = function(){
        var gid=$(e).closest(".cmmt").attr("gid");
        $.post(host+"/cmt/ReplyCmmt", { KeyCode: KeyCode, UserToken: UserToken, gid:gid}, function(){
            ShowCmmtHistory();
        });
    }

    var CancelReply = function(e){
        $(e).closest(".cmmtBody").find("editpanel").remove();
        console.log("cancelReply");
    }
    /* 這邊等於是Public */
    return {
        init: init,
        ShowCmmtHistory: ShowCmmtHistory,
        Save: Save,
        Cancel:Cancel,
        Remove:Remove,
        Reply:Reply,
        ReplySave:ReplySave,
        CancelReply:CancelReply
    };
})();

$(function () {
    Cmmt.init();
});

