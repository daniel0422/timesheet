﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeSheatKPI.ViewModels;
using TimeSheatKPI.Models;
using TimeSheatKPI.Common;

namespace TimeSheatKPI.Controllers
{
    
    public class AjaxController : Controller
    {
        TSheatEntities db;
        HRMIS hrmis = new HRMIS();

        [HttpPost]
        public JsonResult GetWorkingItemDetails(int SN) {
            using (db = new TSheatEntities()) {
                WorkingTitle title = db.WorkingTitles.Where(x => x.SN == SN).SingleOrDefault();
                var wkitem = db.WorkingItems.Where(x => x.PID == title.WorkingItem)
                            .Select(g => new { g.PID, g.Prod, g.Proj, g.Func })
                            .SingleOrDefault();
                var task = db.Tasks.Where(x => x.TaskID == title.TaskID)
                            .Select(g => new { g.TaskID, g.TaskName })
                            .SingleOrDefault();
                return Json(new { wkitem, task });
            };
        }

        [HttpPost]
        public JsonResult GetWorkingProdItems() {
            using (db = new TSheatEntities()) {
                var Entities = db.WorkingItems.GroupBy(x => x.Prod).Select(g => g.Key).ToList();
                return Json(Entities);
            };
        }

        [HttpPost]
        public JsonResult GetWorkingProjItems(string Prod) {
            using (db = new TSheatEntities()) {
                var Entities = db.WorkingItems.Where(x => x.Prod == Prod).GroupBy(x => x.Proj).Select(g => g.Key).ToList();
                return Json(Entities);
            };
        }

        [HttpPost]
        public JsonResult GetWorkingFuncItems(string Prod, string Proj) {
            using (db = new TSheatEntities()) {
                var Entities = db.WorkingItems.Where(x => x.Prod == Prod && x.Proj == Proj)
                    .Select(g=>new { g.Prod, g.Proj, g.Func, g.PID, g.Note, g.Roles }).ToList();
                return Json(Entities);
            };
        }

        [HttpPost]
        public JsonResult GetWorkingTask() {
            using (db = new TSheatEntities()) {
                var es = db.Tasks.OrderBy(x => x.TaskName)
                    .Select(g=>new { g.TaskID, g.TaskName}).ToList();
                return Json(es);
            }
        }

        [HttpPost]
        public JsonResult SaveWkTitle(string mode, int sn, int wkitem, int task, string user, int MM, int YY) {
            string strMM=(MM<10)?"0"+MM.ToString():MM.ToString();
            string YYYYMM = YY.ToString() + strMM;

            using (db = new TSheatEntities()) {

                var iCount = db.vWrokingTitles.Where(x => x.UserID == user && x.WorkingItem == wkitem && x.TaskID == task && x.YYYYMM == YYYYMM && x.IsEnable==null).Count();
                if (mode == "addnew" && iCount>0) {
                    return Json("repeat");
                }
                if (mode == "addnew") {
                    WorkingTitle wt = new WorkingTitle() {
                        UserID = user,
                        MM = MM,
                        YYYY = YY,
                        YYYYMM = YYYYMM,
                        TaskID = task,
                        WorkingItem = wkitem,
                        CreateDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
                    };
                    db.AddToWorkingTitles(wt);
                    db.SaveChanges();

                } else {
                    WorkingTitle wt=db.WorkingTitles.Where(x => x.SN==sn).SingleOrDefault();
                    int oldWkItem=wt.WorkingItem;
                    int oldTaskId=wt.TaskID;
                    wt.WorkingItem=wkitem;
                    wt.TaskID=task;
                    wt.UpdateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    db.SaveChanges();

                    UserLog log = new UserLog() {
                        Action = "Modify",
                        Tb = "WorkingTitle",
                        UserID = user,
                        OldValue = "WorkingItem:" + oldWkItem.ToString() + "\nTaskID:" + oldTaskId.ToString(),
                        NewValue = "WorkingItem:" + wkitem.ToString() + "\nTaskID:" + task.ToString(),
                        UpdateTime = DateTime.Now
                    };
                    db.AddToUserLogs(log);
                    db.SaveChanges();
                }
                
                var vm = db.vWrokingTitles.Where(x => x.UserID == user && x.YYYYMM == YYYYMM).ToList();
                return Json(vm);
            };
        }


        public JsonResult RemoveWkTitle(int sn, string user, int YY, int MM) {
            string strMM = (MM < 10) ? "0" + MM.ToString() : MM.ToString();
            string YYYYMM = YY.ToString() + strMM;

            using (db = new TSheatEntities()) {
                WorkingTitle wt=db.WorkingTitles.Where(x => x.SN == sn).SingleOrDefault();
                wt.IsEnable = "F";
                wt.UpdateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                //db.SaveChanges();

                var es = db.WorkingHourValues.Where(x => x.WkTitleSN == sn).ToList();
                foreach (var c in es) {
                    c.IsEnable = "F";
                    c.UpdateTime = DateTime.Now;
                }
                //db.SaveChanges();

                UserLog log = new UserLog() {
                    Action = "Remove",
                    Tb = "WorkingTitle",
                    UserID = user,
                    OldValue = "WorkingItemSN:" + sn.ToString(),
                    UpdateTime = DateTime.Now
                };
                db.AddToUserLogs(log);
                db.SaveChanges();

                var vm = db.vWrokingTitles.Where(x => x.UserID == user && x.YYYYMM == YYYYMM).ToList();
                return Json(vm);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="MM"></param>
        /// <param name="YY"></param>
        /// <param name="sort">按照什麼排序? 0:a->z, 1:z->a</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ShowWkTitle(string user, int MM, int YY, int sort) {
            string strMM = (MM < 10) ? "0" + MM.ToString() : MM.ToString();
            string YYYYMM = YY.ToString() + strMM;

            using (db = new TSheatEntities()) {
                IQueryable<vWrokingTitle> vm = db.vWrokingTitles.Where(x => x.UserID == user && x.YYYYMM == YYYYMM).AsQueryable();
                switch (sort) {
                    case 0: { vm = vm.OrderBy(x => x.WorkingItemName); break; }
                    case 1: { vm = vm.OrderByDescending(x => x.WorkingItemName); break; }
                } 
                return Json(vm.ToList());
            };
        }

        [HttpPost]
        public JsonResult SaveHour(int WkTitleSN, int YYYY, int MM, int dd, int wkday, string user, decimal? hr) {
            string strMMDD = (MM < 10) ? "0" + MM.ToString() : MM.ToString();
            strMMDD += (dd < 10) ? "0" + dd.ToString() : dd.ToString();
            string YYYYMMDD = YYYY.ToString() + strMMDD;

            try {
                using (db = new TSheatEntities()) {

                    IList<WorkingHourValue> wkhrs = db.WorkingHourValues
                        .Where(x => x.UserID == user && x.RecDate == YYYYMMDD && x.WkTitleSN == WkTitleSN)
                        .ToList();

                    if (wkhrs.Count() == 0) {

                        WorkingHourValue wk = new WorkingHourValue() {
                            WkTitleSN = WkTitleSN,
                            RecDate = YYYYMMDD, //Format:20140914
                            YYYY = YYYY,
                            MM = MM,
                            DD = dd,
                            Hour = hr,
                            UserID = user,
                            WkDay = wkday,
                            UpdateTime = DateTime.Now
                        };
                        db.AddToWorkingHourValues(wk);
                    } else {
                        WorkingHourValue wkhr = wkhrs.SingleOrDefault();
                        wkhr.Hour = hr;
                        wkhr.UpdateTime = DateTime.Now;
                    }
                    db.SaveChanges();
                }
                //var vm = db.vWrokingTitles.Where(x => x.UserID == user && x.YYYYMM == YYYYMM).ToList();
                return Json("Save Hour Success.");

            } catch (Exception ex) {
                string[] ErrArr ={ 
                           WkTitleSN.ToString(),
                           YYYYMMDD,
                           dd.ToString(),
                           hr.ToString(), 
                           Session["UserID"].ToString(), 
                           wkday.ToString() };
                hrmis.SendingErrMsg(ex, ErrArr);
                return Json("Error.");
            };

        }

        [HttpPost]
        public JsonResult ShowHour(int YYYY, int MM, string user) {

            string MMstr = (MM < 10) ? "0" : "",
                YYYYstr = YYYY.ToString(),
                StartDate = YYYY.ToString() + "-" + MMstr + MM.ToString() + "-01",
                EndDate = YYYY.ToString() + "-" + MMstr + MM.ToString() + "-31";

            using (db = new TSheatEntities()) {
                var vm = db.vWorkingHourValues.Where(x => x.UserID == user && x.YYYY == YYYY && x.MM == MM)
                    .Select(g => new {
                        sn = g.WkTitleSN,
                        dd = g.DD,
                        hr = g.Hour
                    }).ToList();

                var offDays = db.vOffDates.Where(x => x.Empno == user || x.Empno == "Public")
                    .Where(x => x.Yearly == YYYYstr && x.C_Date.CompareTo(StartDate) >= 0 && x.C_Date.CompareTo(EndDate) <= 0)
                    .Select(g => new {
                        lvDt =g.C_Date,
                        dscr = g.Description
                    })
                    .OrderBy(x=>x.lvDt)
                    .ToList();
                return Json(new { hrs = vm, offDays = offDays });
            }
        }


    }
}
