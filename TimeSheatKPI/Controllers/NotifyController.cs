﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeSheatKPI.Models;
using TimeSheatKPI.ViewModels;
using TimeSheatKPI.Common;

namespace TimeSheatKPI.Controllers
{
    public class NotifyController : Controller
    {
        //
        // GET: /Notify/
        TSheatEntities TSdb;
        HRMISEntities HRMISdb;
        HRMIS hrmis = new HRMIS();

        public ActionResult Index()
        {
            return View();
        }

        //For Test....
        public ActionResult CalculateDate() {
            
            return View();
        }

        public ActionResult CheckTodayEmail(string mode, string StartDate, string EndDate) {

            EmailContent ec = new EmailContent();
            string IsEnableSendMail = ConfigurationManager.AppSettings["IsEnableSendMail"].ToString(),
                    IsEnableTestEmail = ConfigurationManager.AppSettings["IsEnableTestEmail"].ToString(),
                    MailToTester = ConfigurationManager.AppSettings["MailToTester"].ToString(),
                    path = Server.MapPath("~/Views/Shared/NotifyTemplate.htm"),
                    html = System.IO.File.ReadAllText(path),
                    title = html.Substring(html.IndexOf("<title>") + 7, html.IndexOf("</title>") - html.IndexOf("<title>") - 7),
                    subject = "",
                    body = "",
                    bodys = "",
                    name = "",
                    mailAddress = "",
                    cc = "",
                    mindate = "",
                    datelist = "",
                    errEmail=""; 
            int icc = 0;

            StartDate = (StartDate == null) ? DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd") : StartDate;
            EndDate = (EndDate == null) ? DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") : EndDate;

            if (IsEnableSendMail == "Y") {
                try {

                    using (TSdb = new TSheatEntities()) {
                        //只選取工時不足的部分...
                        var es = TSdb.vAllEmpHrInadequateDetails.Where(x =>
                                                  x.C_Date.CompareTo(StartDate) >= 0 &&
                                                  x.C_Date.CompareTo(EndDate) <= 0
                                                ).AsQueryable();
                        
                        var ms = es.GroupBy(g => g.ID).Select(x => new { User = x.Key}).AsQueryable();
                        //var ms = TSdb.vAllEmpHrInadequateDetailByUsers.OrderBy(x=>x.id).ToList();
                        ec.totalMember = ms.Count().ToString();

                        foreach (var m in ms) {
                            var ee = es.Where(x => x.ID == m.User).OrderBy(x=>x.ID).ThenBy(x=>x.C_Date).ToList();
                            var e1 = es.Where(x => x.ID == m.User).First();
                            errEmail += e1.Email+"<br />";//捕捉Error Msg.
                            mailAddress =e1.Email.Replace(";","");  //clean ";"
                            name = mailAddress.Replace("@onelab.tw", "");   //remove @onelab.tw
                            name = name.Substring(0,1).ToUpper()+name.Substring(1, name.IndexOf(".")-1);
                            icc = ee.Count();
                            cc = icc.ToString();
                            mindate = ee.Min(x=>x.C_Date);
                            datelist = "";

                            foreach (var c in ee) { datelist += c.C_Date.Substring(5,5).Replace("-","/") + ", "; }
                            datelist = datelist.Substring(0, datelist.Length - 2);

                            subject = title.Replace("{{MinDate}}", mindate).Replace("{{CCDays}}", cc);
                            body = html.Replace("{{UserName}}", name)
                                            .Replace("{{CCDays}}", cc)
                                            .Replace("{{MinDate}}", mindate)
                                            .Replace("{{DateList}}", datelist);
                            

                            if (mode == "mail") {
                                using (HRMISdb = new HRMISEntities()) {
                                    A_MailList mail = new A_MailList() {
                                        Subject = subject,
                                        Body = body,
                                        FromDB = "TimeSheet",
                                        CreateTime = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"),
                                        MailFrom = "onelab.mis@onelab.tw",
                                        MailTo = (IsEnableTestEmail == "Y") ? MailToTester : mailAddress,
                                        mGUID = System.Guid.NewGuid().ToString().Replace("-", "")
                                    };
                                    HRMISdb.AddToA_MailList(mail);
                                    HRMISdb.SaveChanges();
                                }
                            } else {
                                bodys += "<hr />To:" + mailAddress + "<br />" + subject + "<br />" + body;
                            }
                        }
                        ec.body = bodys;
                    }
                } catch (Exception ex) {
                    ec.ErrMsg = errEmail+ex.ToString();
                    //hrmis.SendingErrMsg(ex);
                    
                }
                return View(ec);
            }
            return View(ec);
        }


        public void SendEmail() {
            EmailContent ec = new EmailContent();
            
            string IsEnableSendMail = ConfigurationManager.AppSettings["IsEnableSendMail"].ToString(),
                IsEnableTestEmail = ConfigurationManager.AppSettings["IsEnableTestEmail"].ToString(),
                    MailToTester= ConfigurationManager.AppSettings["MailToTester"].ToString(),
                    path = Server.MapPath("~/Views/Shared/NotifyTemplate.htm"),
                    html = System.IO.File.ReadAllText(path),
                    title = html.Substring(html.IndexOf("<title>") + 7, html.IndexOf("</title>") - html.IndexOf("<title>") -7 ),
                    subject = "",
                    body = "",
                    name = "",
                    cc = "",
                    maxdate = "";
            int icc = 0;

            if (IsEnableSendMail == "Y") {
                try {

                    using (TSdb = new TSheatEntities()) {
                        var es = TSdb.vAllEmpHrInadequates
                                    .Where(x => x.FillStatus == "Insufficient").OrderBy(x => x.USerid).ToList();
                        ec.totalMember = es.Count().ToString();
                        
                        foreach (var e in es) {
                            name = e.EmpName;
                            icc = e.workingDays.Value - e.fulfillDays.Value;
                            cc = icc.ToString();
                            maxdate = e.EndDay.ToString();

                            subject = title.Replace("{{MaxDate}}", maxdate);
                            body = html.Replace("{{UserName}}", name)
                                        .Replace("{{CCDays}}", cc)
                                        .Replace("{{MaxDate}}", maxdate);
                            //bodys += "<hr />" + subject + "<br />" + body;
                            using (HRMISdb = new HRMISEntities()) {
                                A_MailList mail = new A_MailList() {
                                    Body = body,
                                    FromDB = "TimeSheet",
                                    Subject = subject,
                                    CreateTime = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"),
                                    MailFrom = "onelab.mis@onelab.tw",
                                    MailTo = (IsEnableTestEmail == "Y") ? MailToTester : e.Email ,
                                    mGUID = System.Guid.NewGuid().ToString().Replace("-","")
                                };
                                HRMISdb.AddToA_MailList(mail);
                                HRMISdb.SaveChanges();
                            }
                        }
                    }
                } catch (Exception ex) {
                    hrmis.SendingErrMsg(ex);
                }
            }
        }




    }
}
