﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using TimeSheatKPI.ViewModels;
using TimeSheatKPI.Models;
using My.Attritube;

namespace TimeSheatKPI.Controllers
{
    public class MainController : Controller
    {

        TSheatEntities db;
        OLPEntities dbOLP;

        public ActionResult Edit(string YY, string MM) {

            //決定目前是要關注哪個月分?
            MM = (String.IsNullOrEmpty(MM)) ? DateTime.Now.Month.ToString() : MM;

            using (db = new TSheatEntities()) 
            {
                if (Session["TSToken"] == null || Session["UserID"]==null) {
                    return RedirectToAction("TransferError");
                } else {
                    MySheatViewModel vm = new MySheatViewModel() {
                        UserID=Session["UserID"].ToString(),
                        CurrMonth = Int16.Parse(MM),
                        WkProdItems = db.WorkingItems.GroupBy(x => x.Prod).Select(g => g.Key).ToList(),
                        Tasks = db.Tasks.OrderBy(x=>x.TaskName).ToList(),
                    };
                    return View(vm);
                }
            };
        }


        [AllowCrossSiteJsonAttribute]
        public ActionResult TransferToTimeSheet(string Token) {
            using (dbOLP = new OLPEntities()) 
            {
                var es = dbOLP.UserRole.Where(x => x.GID == Token);
                if (es.Count() == 0) { return RedirectToAction("TransferError"); }
                UserRole user = es.FirstOrDefault();
                Session["TSToken"] = user.GID;
                Session["UserID"] = user.Account;

                int MM = DateTime.Now.Month;
                return RedirectToAction("Edit", MM);
            }
        }

        public ActionResult ShowAllEmployee() {
            using (db = new TSheatEntities()) {
                IEnumerable<TimeSheatKPI.Models.vEmployee> es 
                    = db.vEmployees.OrderBy(x => x.Account).ToList();
                return View(es);
            }
        }

        public ActionResult TransferError() {
            string OLPUrl = ConfigurationManager.AppSettings["OLP"].ToString();
            return Redirect(OLPUrl);
        }

        public ActionResult PreviewWorkingItems() {
            using (db = new TSheatEntities()) {
                IEnumerable<WorkingItem> es = db.WorkingItems
                    .OrderBy(x => x.Prod)
                    .ThenBy(x => x.Proj)
                    .ThenBy(x => x.Func)
                    .ToList();
                return View(es);
            }
        }

        ///// <summary>
        ///// 這是我測試用的code不用管它!!
        ///// </summary>
        ///// <returns></returns>
        //public ActionResult foo() {
        //    return View();
        //}


        //public ActionResult PivotReport()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public JsonResult GetPivotData(string Dept) {
        //    using (db = new TSheatEntities())
        //    {
        //        var es =
        //            db.vPivot_WorkingHourValues.Where(x => x.Hour > 0)
        //            .Select(g => new
        //            {
        //                g.Hour,
        //                g.YYYY,
        //                g.MM,
        //                g.prod,
        //                g.proj,
        //                g.func,
        //                g.Dept1,
        //                g.Dept2,
        //                g.Dept3,
        //                g.EmpName,
        //                g.Userid,
        //                g.TaskName
        //            }).ToList();

        //        if(Dept=="53950B694A234BA5-987BBA2AE47D8F38")
        //        {
        //            //es.ToList();
        //        }
        //        var jes =Json(es);
        //        jes.MaxJsonLength = int.MaxValue;
        //        return jes;
        //    }
        //}

    }
}
