﻿var mmName = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUY", "AUG", "SEP", "OCT", "NOV", "DEC"];
var mmDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
var wkDays = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
var wkDaysColor = { 0 : { k : 0, n: "Sn", color: "SunColor" },
                  1 : { k : 1, n: "Mo", color: "" },
                  2 : { k : 2, n: "Tu", color: "" },
                  3 : { k : 3, n: "We", color: "" },
                  4 : { k : 4, n: "Th", color: "" },
                  5 : { k : 5, n: "Fr", color: "" },
                  6 : { k : 6, n: "Sa", color: "SatColor"} };
var d = new Date();
var currYY = d.getFullYear();
var currMM = d.getMonth();  /*月*/
var currDate = d.getDate(); /*日*/
var currDay = d.getDay();   /*星期幾*/
var preservMM =2; /* 保證可以看到前幾個月。*/
var user = $("#userid").html();
var iMMM = iMM + 1; /* iMM:JS月份表示法, iMMM實際月份 */
var TimeSheetDisplayMode = 0; /* 0:全部, 1:上半月, 2:下半月*/
var global={mode:"",titlesn:0};
var selectedProd = "",
    selectedProj = "",
    selectedFunc ="",
    selectedPID = "",
    selectedTask = "";

function beginMM() {
    return (TimeSheetDisplayMode < 2) ? 1 : 16;
}

function endMM() {
    return (TimeSheetDisplayMode === 1) ? 15 : mmDays[iMM];
}

$(function () {
    MMTab_init(11);
    /*決定點選的是哪一年?*/
    var arr = location.href.split("/");
    if (arr.length > 5) { currYY = arr[arr.length - 2]; }

    ShowTimeSheat();
    $('.large.modal').modal('show');
    $(".mmviews i:eq(0)").addClass("red");
    console.log("test", arr.length, currYY, currMM, currDate, currDay, wkDaysColor[0]);
});

$(".mmviews i").click(function () {
    var i = $(".mmviews i").index($(this));
    $(".mmviews i").removeClass("red");
    TimeSheetDisplayMode = i;
    $(".mmviews i:eq(" + i + ")").addClass("red");
    ShowTimeSheat();
});


/*--月份標籤初始化--*/
function MMTab_init(mm) {
    var ss = "", i
    , ioldYY_MM = currMM-preservMM+1
    , currYY2=0;

    currYY = d.getFullYear();
    currYY2 = currYY;
    
    /*今年*/
    if (ioldYY_MM <= 0) {
        ioldYY_MM = ioldYY_MM + 11;
        ss += "<b class='item labYY'>" + (currYY - 1) + "</b>"
        for (i = ioldYY_MM; i <= mm; i++) { ss += "<a class='item mm' data-yy=" + (currYY - 1) + " data-mm=" + (i + 1) + ">" + mmName[i] + "</a>"; }
    } else {
        ss += "<b class='item labYY'>" + currYY + "</b>"
        for (i = ioldYY_MM-1; i <= mm; i++) { ss += "<a class='item mm' data-yy=" + currYY + " data-mm=" + (i + 1) + ">" + mmName[i] + "</a>"; }
        currYY2=currYY+1;
    }
    /*明年*/
    if (ioldYY_MM <= 0) {
        ss += "<b class='item labYY'>" + currYY + "</b>";
        for (i = 0; i <= ioldYY_MM - 1; i++) { ss += "<a class='item mm' data-yy=" + currYY + " data-mm=" + (i + 1) + ">" + mmName[i] + "</a>"; }
    } else {
        ss += "<b class='item labYY'>" + currYY2 + "</b>";
        for (i = 0; i <= ioldYY_MM - 2; i++) { ss += "<a class='item mm' data-yy=" + currYY2 + " data-mm=" + (i + 1) + ">" + mmName[i] + "</a>"; }
    }

    /*console.log(mm, "currYY", currYY, "currMM",currMM, preservMM, ioldYY_MM);*/

    $(".header.item").after(ss);
    $(".item[data-mm='"+iMMM+"']").addClass("selected");
    $(".monthly.sidebar").on("click", ".item.mm", function () {
        var me = $(this), arr = [host, "/Main/Edit/0/YM/", me.data("yy"), "/", me.data("mm")];
        /*console.log(host, arr.join(""));*/
        location.href = arr.join("");
    });
}

function ShowTimeSheat() {
    $("#TS .containers").html("");
    $("#TS .containers.body").remove();
    TsContainerHead();
    TsContainerWkDay();
    $.post(host + "/Ajax/ShowWkTitle", { user: user, MM: iMMM, YY: currYY, sort: 1 }, function (rs) {
        var c,d;
        for (c in rs) {
            d = rs[c];
            if (d != null) {
                TsContainer_Body(d.SN, d.WorkingItem, d.TaskID, d.WorkingItemName, d.TaskName);
            }
        }
        ShowWkHour();
    });
    TsContainerFooter();
}

    function TsContainerHead() {
        var ss = "";
        ss += "<div class=wk-title></div><div class='wk-task'></div>";
        for (var i = beginMM(); i <= endMM(); i++) { ss += "<div class='ui dd dd"+i+"'>" + i + "</div>"; }
        $("#TS .header.containers").html(ss);
    }

    function TsContainerWkDay() {
        var ss = "";
        ss += "<div class=wk-title></div><div class='wk-task'></div>";
        for (var i = beginMM(); i <= endMM(); i++) { ss += "<div class='dd " + GetWkDay(i).color + "' data-wkday='" + GetWkDay(i).k + "'>" + GetWkDay(i).n + "</div>"; }
        $("#TS .wkDays.containers").html(ss);
    }

    function TsContainer_Body(sn, wkitemID, taskID, wkitemName, taskName) {
        var ss = "";
        var tmp="<div class='containers body wkitem{2}'><div class='wk-title' data-wkitem='{0}' data-task='{1}' data-titleSN='{2}'><i class='large teal setting link icon no-padding'></i>{3}</div><div class='wk-task'>{4}</div>";
        ss += String.format(tmp, wkitemID, taskID, sn, wkitemName, taskName);

        for (var i = beginMM(); i <= endMM(); i++) { ss += "<div class='dd " + GetWkDay(i).color + " ddhr" + i + "' data-day='" + i + "'></div>"; }
        $("#TS .header.wkDays.containers").after(ss+"</div>");
    }

    function TsContainerFooter() {
        var ss = "";
        ss += "<div class=wk-footer></div><div class='wk-task'></div>";
        for (var i = beginMM(); i <= endMM(); i++) { ss += "<div class='dd ddsumhr" + i + "'></div>"; }
        $("#TS .footer.containers").html(ss);
    }

/*新增按鈕*/
    $(".BtnAddProd").click(function () {
        $(".workingItems.modal").modal("show").find(".updated.wkitem").attr("data-wkitemMode", "addnew").html("");
        initProdDropDown();
        $(".ui.prod.dropdown .text").html("Select Project");
        $(".ui.task.dropdown").dropdown({});
    });

    $(".AddWkItem").click(function () {
        global.mode = "addnew";
        $(".WkItemEdition.modal").modal("show");
        initTaskDropDown().done(function (p) {
            $(".tasks .label:first").click();
        });
        initProdDropDown().done(function (p) {
            $(".prods .label:first").click();
        });
    });

    $(".setting.icon").live("click", function () {
        var me = $(this);
        global.mode = "update";
        global.titlesn = me.parent().data("titlesn");

        $.post(host + "/Ajax/GetWorkingItemDetails", { SN: global.titlesn }, function (rs) {
            var me = $(".WkItemEdition.modal");
            me.modal("show");

            var wkitem = rs.wkitem,
                _Task = rs.task.TaskName,
                _Prod = wkitem.Prod,
                _Proj = wkitem.Proj,
                _Func = wkitem.Func,
                h = "";
            /* console.log(global.titlesn, rs, _Task, _Prod, _Proj, _Func);*/

            initTaskDropDown().done(function (p) {
                getTealElement(".tasks", _Task);
                selectedTask = $(".tasks .teal").data("value");
            });

            initProdDropDown().done(function (p) {
                getTealElement(".prods", _Prod);
                selectedProd = _Prod;
                initProjDropDown(_Prod).done(function (p) {
                    getTealElement(".projs", _Proj);
                    selectedProj = _Proj;
                    initFuncDropDown(_Prod, _Proj).done(function (p) {
                        getTealElement(".funcs", _Func);
                        $(".funcs .teal").click();
                    });
                });
            });


        });

    });

            function getTealElement(es, target) {
                var h;
                return $(es+" .label").removeClass("teal").filter(function () {
                    h = $(this).html();
                    return (h === target && h.length === target.length);
                }).addClass("teal"); 
            }

$("#SaveWorkingTitle").click(function () {
    var wktitlesn = global.titlesn;
    wktitlesn = (global.mode == "addnew") ? -1 : wktitlesn;
    wkitem = selectedPID;
    task = selectedTask;
    /*console.log(global.mode, wktitlesn, "wkitem:", wkitem, "task:", task, user, iMMM, currYY);*/
    $.post(host + "/Ajax/SaveWkTitle", { mode: global.mode, sn: wktitlesn, wkitem: wkitem, task: task, user: user, MM: iMMM, YY: currYY }, function (rs) {
        if (rs == "repeat") {
            alert("您已選過了該工作項目!"); 
        } else {
            ShowTimeSheat();
        }
    });
});

$("#BtnRemoveTitle").click(function () {
    if (confirm("本月該項目的所有工作時數將也會一併移除，您確定?")) {
        var titlesn = global.titlesn;
        /*console.log(titlesn, user, iMMM, currYY);*/
        $.post(host + "/Ajax/RemoveWkTitle", { sn: titlesn, user: user, MM: iMMM, YY: currYY }, function () {
            ShowTimeSheat();
        });
    }
});

/*---Start WkItems edition. ---*/
function initTaskDropDown() {
    return $.post(host + "/Ajax/GetWorkingTask/", {}, function (rs) {
        var ss = "", t, d;
        for (t in rs) { d = rs[t]; ss += "<b class='ui label' data-value='" + d.TaskID + "'>" + d.TaskName + "</b>"; }
        $(".WkItemEdition .tasks").html(ss);
    }).pipe(function (p) {
        return p;
    });
}
function initProdDropDown() {
    return $.post(host + "/Ajax/GetWorkingProdItems", {}, function (rs) {
        var ss = "", t, d;
        for (t in rs) { d = rs[t]; ss += "<b class='ui label'>" + d + "</b>"; }
        $('.WkItemEdition .prods').html(ss);
    }).done(function (p) {
        return p;
    });
}

function initProjDropDown(vd) {
    return $.post(host + "/Ajax/GetWorkingProjItems", { Prod: vd }, function (rs) {
        var ss = "", t, d;
        for (t in rs) { d = rs[t]; ss += "<b class='ui label'>" + d + "</b>"; }
        $('.WkItemEdition .projs').html(ss);
    }).done(function (p) {
        return p;
    });
}

function initFuncDropDown(vd, vj) {
    return $.post(host + "/Ajax/GetWorkingFuncItems/", { Prod: vd, Proj: vj }, function (rs) {
        var ss = "", t, d;
        for (t in rs) { d = rs[t]; ss += "<b class='ui label' data-value='" + d.PID + "'>" + d.Func + "</b>"; }
        $(".WkItemEdition .funcs").html(ss);
    }).done(function (p) {
        return p;
    });
}


        $(".prods").on("click", ".label", function () {
            var me = $(this),
                vd = $(this).html();
            selectedProd = vd;
            me.siblings().removeClass("teal");
            me.addClass("teal");
            initProjDropDown(vd).done(function () {
                $(".projs .label:first").click();
            });
        });

        $(".projs").on("click", ".label", function () {
            var me = $(this),
               vj = me.html();
            selectedProj = vj;
            me.siblings().removeClass("teal");
            me.addClass("teal");
            initFuncDropDown(selectedProd, vj).done(function () {
                $(".funcs .label:first").click();
            });
        });

        $(".funcs").on("click", ".label", function () {
            var me = $(this);
            selectedFunc = me.html();
            selectedPID = me.data("value");
            me.siblings().removeClass("teal");
            me.addClass("teal");
        });

        $(".tasks").on("click", ".label", function () {
            var me = $(this);
            selectedTask = me.data("value");
            me.siblings().removeClass("teal");
            me.addClass("teal");
        });


/*---End WkItems edition. ---*/
        $(".containers.body .dd").live("click", function (e) {
            var me = $(this),
                v,
                wkhh,
                offset,
                top = 0,
                left = 0;

            $("#txtWkHour").remove();
            v = me.html();
            offset = me.offset();
            top = offset.top + 4;
            left = offset.left - 8;
            wkhh = $(".txtWkHour.hidden").clone().removeClass("hidden").addClass("actived").attr("id", "txtWkHour");
            me.attr("data-oldHH", v).append(wkhh);
            $("#txtWkHour").offset({ top: top, left: left });

            /* $(".selectedRow").removeClass("selectedRow"); */
            /* me.siblings().addClass("selectedRow").parent().parent().addClass("selectedRow");*/
            
            $(".selectedRow2").removeClass("selectedRow2");
            me.parent().addClass("selectedRow2");
            /* console.log(".dd click:", v, top, left); !! */

            $("#txtWkHour").val(v).focus()
    .focusout(function () {
        TxtWkHourPatchSheat($(this));
        $(".dd.selectedRow").removeClass("selectedRow");
    })
    .keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            TxtWkHourPatchSheat($(this));
            $(".dd.selectedRow").removeClass("selectedRow");
        }
    }).select();

        });

    function TxtWkHourPatchSheat(me) {
        var v = me.val(),
            pa = me.parent(),
            oldHH = pa.data("oldhh"),
            patt = /^\d+\.?\d{0,2}$/;
        
        /* console.log("v:", v, typeof(v),", oldHH:", oldHH, typeof(oldHH), v === oldHH); !! */
        pa.attr("data-oldHH", v);

        if (v == "") {
            pa.html(oldHH);
            SaveWkHour(pa, me, 0);
        } else {
            v = Number(v);
            if (IsRegExpValid(patt, v)) {
                pa.html(v);    
                if (v != oldHH) {
                    SaveWkHour(pa, me, v);
                }
            } else {
                me.addClass("errorFormat");
            }
        }
    }

    function SaveWkHour(pa, me, hr) {
        var title = $(pa).parent().find(".wk-title");
        var WkTitleSN=title.data("titlesn");
        var yyyy = currYY;
        var mm = iMMM;
        var dd = pa.data("day");
        var idd = dd - 1;
        idd = (TimeSheetDisplayMode == 2) ? idd - 15 : idd;  /* 切上下半月要注意!! */
        var wkday = $(".containers.header.wkDays .dd:eq(" + idd + ")").data("wkday"); /*星期幾?*/

        pa.addClass("edited");
        /*console.log("WkTitleSN:",WkTitleSN, yyyy, mm, dd, wkday, user, hr);*/
        $.post(host + "/Ajax/SaveHour", { WkTitleSN: WkTitleSN, YYYY: yyyy, MM: mm, dd: dd, wkday: wkday, user: user, hr: hr }, function (rs) {
            ShowWkHour();
        });
    }

    function ShowWkHour() {
        $.post(host + "/Ajax/ShowHour", { YYYY: currYY, MM: iMMM, user: user }, function (rs) {
            var d,
                s = "",
                floatd = 0.0,
                c,
                hrs = rs.hrs,
                offDays = rs.offDays;

            for (c in hrs) {
                d = hrs[c];
                floatd = d.hr;
                s = (d.hr == 0) ? "" : floatd.toFixed(2);
                $(".containers.body.wkitem" + d.sn).find(".dd.ddhr" + d.dd).html(s);
            }

            var sum = 0.0,
                hr = "",
                k = 0,
                me,
                dscr = "",
                lvDt = "",
                lvInfor = "";

            $(".lvlabel").remove();
            for (k = 1; k <= 31; k++) {
                sum = 0.0;
                lvInfor = isDayHoliday(offDays, k);

                if (lvInfor !== "") {
                    $(".containers.header .dd" + k)
                        .append("<div class='small pointing red ui black below label lvlabel'><div class='text'>Leave</div></div>")
                        .addClass("lvDt").attr("title", lvInfor)
                        ;
                }

                $(".ddhr" + k).each(function (i, n) {
                    me = $(n);
                    if (lvInfor !== "") { me.addClass("lvDt").attr("title", lvInfor); }  /* 列出HRMIS請假資訊. */

                    hr = me.html();
                    hr = (hr == "") ? "0" : hr;
                    sum += parseFloat(hr);
                    /*console.log(i, hr, sum);*/
                    $(".dd.ddsumhr" + k).html((sum > 0) ? sum.toFixed(2) : "");
                });
                /*console.log(".dd.ddsumhr" + i, sum);*/
            }
        });
    }

    function isDayHoliday(offDays, iDate) {
        var j = 0,
            jlen = offDays.length,
            iMMMstr = (iMMM < 10) ? "0" : "",
            yyyymm = currYY + "-" + iMMMstr + iMMM + "-",
            yymmdd ="",
            iDateStr = "",
            lvDt="",
            dscr=""
            ;
        for (j = 0; j <= jlen; j++) {
            iDateStr = (iDate < 10) ? "0" : "";
            yymmdd = yyyymm + iDateStr + iDate;
            /* console.log(iMMM < 10, iMMM, typeof (iMMM), "ddas:", yymmdd); */
            if (offDays[j]) {
                lvDt = offDays[j].lvDt;
                if (lvDt === yymmdd) { /*console.log(yymmdd, lvDt);*/
                    dscr = offDays[j].dscr;
                    break;
                }
            }
        }
        return dscr;
    }

    function IsRegExpValid(patt, v) {
        return (patt.test(v) && v < 24);
    }

    function GetWkDay(iDate) {
        var YYYYMMDD = "", day, iday;
        YYYYMMDD = currYY + "/" + iMMM + "/" + iDate;
        iday = new Date(YYYYMMDD).getDay();
        return wkDaysColor[iday];
    }