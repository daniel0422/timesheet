module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                //banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                mangle: {
                    except: ['Backbone']
                }
            },
            my_target: {
                files: {
                    "js/dest/m1504.js": [
                        "views/Main.js"
                    ]
                }
            }
        },
        cssmin: {
            combine: {
                options: {
                    banner: "/* Minified css file by Daniel.Chou */"
                },
                files: {
                    "css/dest/m.css": [
						"css/Site.css",
						"js/vendor/semantic-ui/build/packaged/css/semantic.css",
						"css/m.css"
						]
                }
            }
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapesWhitespace: true
                },
                files: {
                    "../17Y-Reunion/src/index.html":"../17Y-Reunion/index.html"
                }
            }
        }


    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');

    // Default task(s).
    grunt.registerTask('default', ['uglify', "cssmin"]);

};