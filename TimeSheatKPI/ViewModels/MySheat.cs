﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeSheatKPI.Models;

namespace TimeSheatKPI.ViewModels {

    public class MySheatViewModel 
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public int CurrMonth { get; set; }
        public IEnumerable<string> WkProdItems { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        public IEnumerable<WorkingTitle> WorkingTitles { get; set; }
        public IEnumerable<WorkingHourValue> WorkingHourValues { get; set; }
    }

    public class UserViewModel {
        public string UserID { get; set; }
        public string UserName { get; set; }
    }
}