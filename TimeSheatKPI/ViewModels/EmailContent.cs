﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeSheatKPI.ViewModels {
    public class EmailContent {
        public string totalMember { get; set; }
        public string body { get; set; }
        public string ErrMsg { get; set; }
    }
}