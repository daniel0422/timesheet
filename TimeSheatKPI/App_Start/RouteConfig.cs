﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TimeSheatKPI {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //後門路徑-
            routes.MapRoute(
             name: "BackDoor", 
             url: "BackDoor/0/3239BE2945BD904F",
             defaults: new { controller = "Main", action = "ShowAllEmployee", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "MainEdit",
               url: "{controller}/{action}/0/YM/{YY}/{MM}",
               defaults: new { controller = "Main", action = "Edit", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Main", action = "Edit", id = UrlParameter.Optional }
            );
        }
    }
}