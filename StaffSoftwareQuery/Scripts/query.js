﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../bower_components/",
    paths: {
        'vue': ['vue/dist/vue.min'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/core"],
        "datepicker": ["jqueryui/ui/datepicker"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "jquery", "datepicker"], function (Vue, $) {
    var $loading = $('#loadingDiv').hide();
    $(document).ajaxStart(function () {
        $loading.show();
    })
    .ajaxStop(function () {
        $loading.hide();
    });
    var host = function (u) {
        return "/Staff/" + u;
    }

    Vue.debug = true;
    /* Dress list component. */
    Vue.component("member-list", {
        template: "#memberlist-template",
        data: function () {
            return {
                members: [],
                searchText: "",
                icountMembers: 0,
                selectedMemberID: ""
            }
        },
        created: function () {
            this.fetchData();
            this.$watch('searchText', function () {
                console.log(this._children);
                this.icountMembers = this._children.length;
            });
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("GetSoftwareData"), {}, function (rs) {
                    self.members = rs.members;
                    self.icountMembers = self.members.length;
                    self.$emit("list-loaded");
                });
            },
            clickImport2DB: function () {
                $.post(host("Write2DB"), {}, function (rs) {
                    var msg = (rs == "OK") ? "轉換完成!" : "轉換有問題!";
                    alert(msg);
                });
            }
        }
    });

    var main = new Vue({
        el: "#main",
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); }
        }
    });
});
