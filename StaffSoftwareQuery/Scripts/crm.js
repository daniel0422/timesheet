﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../../bower_components/",
    paths: {
        'vue': ['validator/vendor/vue-0.11.1'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/core"],
        "datepicker": ["jqueryui/ui/datepicker"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    Vue.debug = true;
    Vue.use(validator);
    Vue.directive('datepicker', {
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy/mm/dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    /* Dress list component. */
    Vue.component("member-list", {
        template: "#memberlist-template",
        data: function () {
            return {
                members: null,
                searchText: "",
                icountMembers: 0,
                selectedMemberID: "",
            }
        },
        compiled: function () {
            this.fetchData();
            this.$watch('searchText', function () {
                console.log(this._children);
                this.icountMembers = this._children.length;
            });
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("Member/GetList"), {}, function (rs) {
                    self.members = rs.members;
                    self.icountMembers = self.members.length;
                    self.$emit("list-loaded");
                });
            },
            clickAddNewDress: function () {
                main.editMode = true;
                main.isDetail = true;
                this.$parent.$.refMemberForm.AddNewMember();
            },
            getOneMember: function (e) {
                main.editMode = false;
                var self = this;
                var raw = e.targetVM._raw;
                var gid = raw.GID;         //Pass to parent...
                this.selectedMemberID = gid;
                main.MemberID = gid;
                main.isDetail = true;
                this.$parent.$.refMemberForm.MemberID = gid;
                //console.log("list", gid, this.selectedMemberID, "|", this.$parent.$);
            }
        }
    });

    Vue.component("member-form", {
        template: "#member-form-template",
        //replace: true, // cache view...
        data: function(){
            return {
                Member: { GID: "", CallName: "", Mate: "", Email: "", TEL: "", Mobile: "", Address: "", WeddingDate: "", EngageDate: "", InsertDate: "", UpdateTime: "", Birthday: "", Note: "" },
                editMode: false,
                MemberID: "",
                isDetail:false
            }
        },
        created: function () {
            this.$watch("MemberID", function () {
                var gid = main.MemberID;
                this.fetchData();
            });
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("Member/GetOneMember"), { gid: main.MemberID }, function (rs) {
                    var f = self.Member;
                    f.GID = rs.GID;
                    f.CallName = rs.CallName;
                    f.Email = rs.Email;
                    f.Address = rs.Address;
                    f.TEL = rs.TEL;
                    f.Note=rs.Note;
                    f.Mobile = rs.Mobile; console.log(rs);
                    f.WeddingDate = rs.WeddingDate;
                    f.EngageDate = rs.EngageDate;
                    f.InsertDate = rs.InsertDate;
                    f.UpdateTime = rs.UpdateTime;
                    f.Birthday = rs.Birthday;
                });
            },
            hideDetail: function () {
                main.isDetail = false;
            },
            AddNewMember: function () {
                console.log('addnew');
                this.editMode=true;
                this.Member = { GID: "", CallName: "", Mate: "", Email: "", TEL: "", Mobile: "", Address: "", WeddingDate: "", EngageDate: "", InsertDate: "", UpdateTime: "", Birthday: "", Note: "" };
            },
            clickEdit: function () {
                this.editMode = true;
            },
            clickSave: function (e) {
                e.preventDefault();
                var self = this;
                $.post(host("Member/SaveMember"), this.Member, function (rs) {
                    self.fetchData();
                    self.editMode = false;
                    self.$parent.$.refMemberlist.fetchData();
                });
            },
            clickCancel: function () {
                this.editMode = false;
            }
        }

    });

    var main = new Vue({
        el: "#main",
        data: {
            editMode: false,
            MemberID: "",
            isDetail: false
        },
        created: function () {
            this.$watch("MemberID", function () {
                //console.log("main", this.MemberID);
            })
        },
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); }
        },
        methods: {
          
        }
    });



});
