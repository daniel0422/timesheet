﻿
/*Like .net GUID()*/
function newGuid() {
    var guid = "";
    for (var i = 1; i <= 32; i++) {
        var n = Math.floor(Math.random() * 16.0).toString(16);
        guid += n;
        if ((i == 8) || (i == 12) || (i == 16) || (i == 20))
            guid += "-";
    }
    return guid;
}

function newUniqueID(iLength, UserName) {
    var id = "";
    for (var i = 1; i <= iLength; i++) {
        id += Math.floor(Math.random() * 32.0).toString(32);
    }
    return id;
}

var USKey = "";


function fn_sep(s) {
    var regex = /|/gi;
    return s.replace(regex, "[js-sep]");
}


    String.format = function () {
        var s = arguments[0];
        if (s == null) return "";
        for (var i = 0; i < arguments.length - 1; i++) {
            var reg = getStringFormatPlaceHolderRegEx(i);
            s = s.replace(reg, (arguments[i + 1] == null ? "" : arguments[i + 1]));
        }
        return cleanStringFormatResult(s);
    }
    /*可在Javascript中使用如同C#中的string.format (對jQuery String的擴充方法)
        使用方式 : var fullName = 'Hello. My name is {0} {1}.'.format('FirstName', 'LastName');*/
    String.prototype.format = function () {
        var txt = this.toString();
        for (var i = 0; i < arguments.length; i++) {
            var exp = getStringFormatPlaceHolderRegEx(i);
            txt = txt.replace(exp, (arguments[i] == null ? "" : arguments[i]));
        }
        return cleanStringFormatResult(txt);
    }

    String.prototype.ToJSDate =function(value) {
        var pattern = /Date\(([^)]+)\)/;
        var results = pattern.exec(value);
        var dt = new Date(parseFloat(results[1]));
        return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
    }
    
    /*讓輸入的字串可以包含{}*/
    function getStringFormatPlaceHolderRegEx(placeHolderIndex) {
        return new RegExp('({)?\\{' + placeHolderIndex + '\\}(?!})', 'gm')
    }
    /*當format格式有多餘的position時，就不會將多餘的position輸出
    //ex:
    // var fullName = 'Hello. My name is {0} {1} {2}.'.format('firstName', 'lastName');
    // 輸出的 fullName 為 'firstName lastName', 而不會是 'firstName lastName {2}'*/
    function cleanStringFormatResult(txt) {
        if (txt == null) return "";
        return txt.replace(getStringFormatPlaceHolderRegEx("\\d+"), "");
    }

    function ToJavaScriptDate(value) {
        var pattern = /Date\(([^)]+)\)/,
             results = pattern.exec(value);
        if (value === null || results==null || value === "") {
            return "";
        }
        var dt = new Date(parseFloat(results[1])),
             dd = dt.getDate(),
             mm = dt.getMonth() + 1;

        mm = (mm < 10) ? "0" + mm : mm;
        dd = (dd < 10) ? "0" + dd : dd;
        return dt.getFullYear() + "/" + mm + "/" + dd;
    }

    function ToJavaScriptDateTime(value) {
        var pattern = /Date\(([^)]+)\)/,
             results = pattern.exec(value);
        if (value === null || results == null || value === "") {
            return "";
        }
        var dt = new Date(parseFloat(results[1])),
             dd = dt.getDate(),
             mm = dt.getMonth() + 1,
             hh = dt.getHours(),
             min = dt.getMinutes(),
             sec = dt.getSeconds();

        mm = (mm < 10) ? "0" + mm : mm;
        dd = (dd < 10) ? "0" + dd : dd;
        return dt.getFullYear() + "/" + mm + "/" + dd+" "+hh+":"+min+":"+sec;
    }

    function host(url) {
        return $(".brand-name").attr("href") + url;
    }


