﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../../bower_components/",
    paths: {
        "jquery": ['jquery/dist/jquery.min'],
        'vue': ['vue/dist/vue.min'],
        'validator': ['validator/validator'],
        "core": ["jqueryui/ui/core"],
        "datepicker": ["jqueryui/ui/datepicker"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    var hostUrl = "http://danielchou-001-site1.smarterasp.net";
    var hostImageUrl = $("#imgHostUrl").val();
    Vue.debug=true;
    Vue.use(validator);
    Vue.directive('datepicker', {
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    Vue.component("dress-img-picker", {
        template: "#dressImg-Picker-Template",
        data: function () {
            return {
                dressImgs: null,
                searchImgs: "",
                icountPicked: 0,
                PickedDress: [],
                PickedDressOrigin: []
            }
        },
        compiled: function () {
            this.fetchData();
            this.$watch('searchImgs', function () {
                this.icountPicked = this.PickedDress.length;
            });
        },
        methods:{
            fetchData: function () {
                var self = this;
                $.post("/Admin/ShowAllDressMasterImage", {}, function (rs) {
                    self.dressImgs = rs;
                    self.$emit("imgs-loaded");
                });
            },
            OnPickedDress: function (dress) {
                var self=this;
                var d = dress.$data;
                self.PickedDressOrigin.push(d);
                self.dressImgs.$remove(d);
                self.icountPicked = self.PickedDressOrigin.length;
            },
            RemovePickedDress: function (dress) {
                var self = this;
                var d = dress.$data;
                self.PickedDressOrigin.$remove(d);
                self.dressImgs.unshift(d);
                self.icountPicked = self.PickedDressOrigin.length;
                //console.log(self.PickedDressOrigin);
            }
        }
    });

    Vue.component("rsrv-main", {
        template: "#Rsrv-Main-tmeplate",
        data: function () {
            return {
                RsrvID:"",
                RsrvData: { SN: "", TYP: "", eventid: "", CustomerID: "", StartDate: "", EndDate: "", StartDateTime:"",EndDateTime:"", InsertDate: "", Note: "", IsEnable: true },
                Options: { memberOptions: null },
                currDayTyp: "all",
                dayTyps:{
                    all: { k:"all", n: "全天", dt1:"08:00:00", dt2 :"22:00:00" },
                    am : { k:"am", n: "上午", dt1:"08:00:00", dt2 :"12:00:00" },
                    pm : { k:"pm", n: "下午", dt1:"12:00:00", dt2 :"18:00:00" },
                    night: { k:"night", n: "晚上", dt1:"18:00:00", dt2 :"22:00:00" },
                    cross: { k:"cross", n: "跨日", dt1:"08:00:00", dt2 :"18:00:00" }
                },
                RsrvTyp: null,
                currRsrvTypID: "EBE13137",  //Todo: get init data from DOM.
                isEndDate:false
            }
        },
        created: function () {
            this.RsrvID = document.getElementById("RsrvEditor").getAttribute("rsrvid");
            this.GetMemberOptions();
            if (this.RsrvID != "") { this.GetRsrvDetails(); }
        },
        compiled: function () {
            this.$watch("RsrvData.StartDate + RsrvData.EndDate", function () {
                this.DayTypHandler();
            });
        },
        methods: {
            GetRsrvDetails: function () {
                var self = this;
                $.post("/Calendar/GetRsrvDetails", { RsrvID: this.RsrvID }, function (rs) {
                    self.RsrvData = rs.ReservDate;
                    main.$.dressImgsPicker.PickedDressOrigin = rs.RsrvDetails;
                });
            },
            GetMemberOptions: function () {
                var self = this;
                $.post("/Calendar/GetMemberOptions", {}, function (rs) {
                    self.Options.memberOptions = rs.memberOptions;
                    self.RsrvTyp = rs.RsrvDetailTyp;
                });
            },
            clickRsrvTyp: function (e) {
                var raw = e.targetVM._raw; console.log(raw.gid);
                this.currRsrvTypID = raw.gid;
            },
            clickDayType: function (e) {
                var t = e.targetVM._raw;
                this.currDayTyp = t;
                this.isEndDate = (this.currDayTyp.k == "cross");
                this.DayTypHandler();
            },
            DayTypHandler: function () {
                var me=this;
                me.RsrvData.StartDateTime = me.RsrvData.StartDate + " " + me.currDayTyp.dt1;
                me.RsrvData.EndDateTime = ((me.isEndDate) ? me.RsrvData.EndDate : me.RsrvData.StartDate) + " " + this.currDayTyp.dt2;
                console.log(me.currDayTyp, me.RsrvData.StartDateTime, me.RsrvData.EndDateTime);
            },
            SaveRsrvDate: function () {
                var RsrvDetails = this.$parent.$.dressImgsPicker.PickedDressOrigin;
                var self = this;
                $.post("/Calendar/SaveRsrvDate", { RsrvData: self.RsrvData, RsrvDetail: RsrvDetails }, function (rs) {

                });
            },
        }
    });

    var main = new Vue({
        el: "#RsrvEditor",
        data: {},
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); }
        }
    });
   
});

