﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../../bower_components/",
    paths: {
        'vue': ['validator/vendor/vue-0.11.1'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/core"],
        "datepicker": ["jqueryui/ui/datepicker"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    Vue.debug=true;
    Vue.use(validator);
    Vue.directive('datepicker', {
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy/mm/dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    Vue.component("dress-list", {
        template: "#dresslist-template",
        paramAttributes:["gid"],
        data: function () {
            return {
                dresses: null,
                cmp_searchText: "",
                icountDress: 0
            }
        },
        compiled: function () {
            this.fetchData();
            this.$watch('cmp_searchText', function () {
                this.icountDress= this._children.length;
            });
        },
        computed:{
            isSelected: function () {
                console.log(this.model);
                return this.dresses.gid==this.gid;
            },
            isTest: function () {
                return ture;
            }
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("admin/GetDressList"), {}, function (rs) {
                    self.dresses = rs;
                    self.icountDress = self.dresses.length;
                    self.$emit("dress-loaded");
                });
            },
            getOneDress: function (e) {

                
                var self = this;
                var fm = self.$parent.$.dressfm; // = false;
                //console.log("list compo:", main.isAddNew, fm);
                if (fm) {
                    //console.log("list:", fm.editMode);
                    fm.isAddNew = false;
                    fm.editMode = false;
                }

                $("div.dress").removeClass("selected");
                $(e.target.parentNode).addClass("selected");
                $("#dressForm").find("input,select").attr("disabled", "disabled");
                var raw=e.targetVM._raw;
                main.gid = raw.gid;         //Pass to parent...
                main.masterImg = raw.img2;  //Pass to parent...
            }
        }
    });

    Vue.component("dress-form",{
        template: "#dress-form-template",
        validator: {},
        data: function(){
            return {
                editMode: false, /* view|addnew|update */
                dress: {
                    GUID: "",ID: 0, Color: 1, Typ: 19, Name: "", Code: "", Hanger: 7, VenderID: 57, PerchaseDate: "", Slogan: "", OpenDate: "", OffDate: "", Cost: 0, IsEnabled:"N",InsertDate: null, UpdateTime: null
                },
                options: {},
                gid: "",
                isAddNew:false
            };
        },
        created: function () {
            this.editMode = false;
            this.$watch("gid+isAddNew+editMode", function () {
                this.checkDressMode();
            });
        },
        methods: {
            checkDressMode: function () {
                var self = this;
                if (self.editMode) {
                    $("#dressForm").find("input,select").removeAttr("disabled");
                } else {
                    $("#dressForm").find("input,select").attr("disabled", "disabled");
                }
                if (this.isAddNew) {
                    this.AddNewDress();
                } else {
                    this.getOneDressDetail();
                }
            },
            clickSaveDress: function (e) {
                e.preventDefault();
                var self = this;
                this.editMode = false;
                if (self.$valid) {
                    $.post(host("admin/SaveDress"), self.dress, function (rs) {
                        self.$parent.$.refDressList.fetchData();
                        main.gid;
                        self.getOneDressDetail();
                    });
                }
            },
            clickEditDress: function () {
                this.editMode = true;
            },
            clickCancelEdit:function(e){
                this.editMode = false;
            },
            getOneDressDetail: function () {
                var self = this;
                $.post(host("admin/GetDressDetails"), { gid: main.gid }, function (rs) {
                    var f = self.dress
                    f.GUID = rs.GUID;
                    f.ID = rs.ID;
                    f.Name = rs.Name;
                    f.Slogan = rs.Slogan;
                    f.Cost = rs.Cost;
                    f.Code = rs.Code;
                    f.OffDate = ToJavaScriptDate(rs.OffDate);
                    f.OpenDate = ToJavaScriptDate(rs.OpenDate);
                    f.PerchaseDate = ToJavaScriptDate(rs.PerchaseDate);
                    f.InsertDate = ToJavaScriptDate(rs.InsertDate);
                    f.UpdateTime = ToJavaScriptDate(rs.UpdateTime);
                    f.VenderID = rs.VenderID;
                    f.IsEnabled = rs.IsEnabled;
                    f.Color = rs.Color;
                    f.Hanger = rs.Hanger;
                    f.Typ = rs.Typ;
                });
            },
            AddNewDress: function (e) {
                $("#dressForm").find("input,select").removeAttr("disabled");
                var self = this;
                self.editMode = true;
                $.post("/Admin/GetStoreMaxCode", {}, function (rs) {
                    self.dress = {
                        GUID: "", ID: 0, Color: 1, Typ: 19, Name: "", Code: "", Hanger: 7, VenderID: 57, PerchaseDate: "", Slogan: "", OpenDate: "", OffDate: "", Cost: 0, IsEnabled: "N", InsertDate: null, UpdateTime: null
                    };
                    self.dress.Code = rs;
                });
            }
        }
    });


    Vue.component("dress-img", {
        template: "#dress-img-template",
        validator: {},
        data: function () {
            return {
                editMode: false, /* view|addnew|update */
                gid: main.gid,
                imgs: null,
                masterImg: main.masterImg,
                isFrei: false,
                isLoading: false,
                isLoadingError:false
            };
        },
        created: function () {
            this.fetchData();
            this.$watch("gid", function () {
                this.fetchData();
            });
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post("/Admin/ShowOneDressImages", { DressGuid: this.gid }, function (rs) {
                    self.imgs = rs;
                });
            },
            saveFile: function () {
                var fd = new FormData();
                var blob = document.getElementById("files").files[0];
                var token = document.getElementsByName("__RequestVerificationToken")[0].value;
                var dress_gid = document.getElementById("DressGid").value;
                fd.append("files", blob);
                fd.append("__RequestVerificationToken", token);
                fd.append("dress_gid", dress_gid);
                fd.append("UploadTyp", "DressPhoto");
                var self = this;

                $.ajax({
                    url: "/Admin/UploadFile",
                    type: "POST",
                    data: fd,
                    beforeSend: function () { self.isLoading = true; },
                    success: function () { self.fetchData();  self.isLoading = false; },
                    error: function () { self.isLoadingError = true; },
                    processData: false,  // tell jQuery not to procefss the data
                    contentType: false   // tell jQuery not to set contentType
                });
            },
            deleteImg: function (e) {
                var raw = e.targetVM._raw,
                    imgGuid = raw.Guid,
                    self = this;
                if (confirm("確認要刪除" + raw.FileNameNew + "?")) {
                    $.post("/Admin/RemoveFile", { imgGuid: imgGuid }, function (rs) {
                        if (rs == "RemoveFile Success.") {
                            alert("刪除成功");
                            self.fetchData();
                        }
                    });
                }
            },
            editImg: function (e) {
                var raw = e.targetVM._raw,
                    imgGuid = raw.Guid,
                    Note = raw.Note,
                    self = this;
                $.post("/Admin/EditAttachFileNote", { imgGuid: imgGuid, note:Note }, function (rs) {
                    self.fetchData();
                });
            },
            onMasterImg: function (e) {
                var raw = e.targetVM._raw,
                    imgGID = raw.Guid,
                    selectedImg = raw.FileNameNew,
                    dressGID = this.gid,
                    self = this;
                    
                $.post("/Admin/SetMasterImage", { dressGID: dressGID, imgGID: imgGID }, function (rs) {
                    //console.log(self.$parent.$);
                    var llist = self.$parent.$.refDressList;
                    llist.fetchData();
                    llist.cmp_searchText=dressGID;
                    main.masterImg = selectedImg;
                });
            }

        }
    });


    var main = new Vue({
        el: "#main",
        data: {
            tabs: {
                detail: { n: "禮服明細", b: "active" },
                pics: { n: "圖片管理", b: "" },
                crm: { n: "客戶使用紀錄", b: "" },
                infor: { n: "使用狀況", b: "" }
            },
            currTab: "",
            dresses: null,
            colorOptions: null,
            field: "name",
            options:{
                colorOptions: null, 
                typOptions: null,
                hangerOptions: null,
                venderOptions: null
            },
            gid: "",
            isDressForm: true,
            isDressImg: false,
            masterImg: "",
            isFrei: false
        },
        created: function () {
            this.isFrei = ($(".brand-name").html() == "frei") ? true : false;
            this.GetDressOptions();
            var self = this;
            this.$watch("currTab", function () {
                this.clickTabEE();
            });
            
        },
        filters: {
            formatDate: function (v) {
                return ToJavaScriptDate(v);
            }
        },
        methods: {
            GetDressOptions: function () {
                var op = this.options;
                var self = this;
                $.post(host("admin/GetDressOptions"), {}, function (rs) {
                    op.colorOptions = rs.colorOptions;
                    op.typOptions = rs.typOptions;
                    op.hangerOptions = rs.hangerOptions;
                    op.venderOptions = rs.venderOptions;
                });
            },
            clickTab: function (e) {
                var raw = e.targetVM._raw;
                for (var c in this.tabs) { this.tabs[c].b = ""; }
                raw.b = "active";
                this.currTab = e.targetVM.$key;
                this.clickTabEE();
                this.editMode = false;
                //console.log(raw, this.currTab, e.targetVM.$key);
            },
            clickTabEE: function () {
                this.isDressForm = (this.currTab == "detail");
                this.isDressImg = (this.currTab == "pics");
            },
            clickAddNewDress: function () {
                this.gid = "  ";
                this.isDressImg = false; //hierachy比較高!
                this.isDressForm = true;
                this.editMode = false;
                this.isAddNew = true; //console.log("parent:", this.isAddNew);
                this.$.dressfm.isAddNew = true;  //必要!
                for (var c in this.tabs) { this.tabs[c].b = ""; }
                this.tabs.detail.b = "active";
                this.currTab == "detail";
                
            }
        }
    });

    

});
