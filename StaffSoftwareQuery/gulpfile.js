var gulp =require("gulp"),
	connect = require("gulp-connect"),
    cssmin = require('gulp-cssmin'),
	$ = require("gulp-load-plugins")();  /* https://www.npmjs.com/package/gulp-load-plugins */

gulp.task("js-min",function(){
	return gulp.src([
		"bower_components/jquery/dist/jquery.js",
		"bower_components/semantic-ui/dist/semantic.js",
		"bower_components/moment/moment.js",
		"bower_components/fullcalendar/dist/fullcalendar.js",
		"bower_components/vue/dist/vue.js",
	])
	.pipe($.jshint())
	.pipe($.jshint.reporter("default"))
	.pipe($.uglify())
	.pipe($.concat("m5.js"))
	.pipe(gulp.dest("Content/dist"));
});

//這邊是公用的code 開發版
gulp.task("js-layout-dev", function () {
    return gulp.src([
		"bower_components/jquery/dist/jquery.js",
        "bower_components/bootstrap/dist/js/bootstrap.js",
        "bower_components/vue/dist/vue.js",
        "bower_components/vue-validator/dist/vue-validator.js",
        "Scripts/myPlugin.js"
    ])
	.pipe($.concat("entry-dev.js"))
	.pipe(gulp.dest("Scripts/dist"));
});

gulp.task("js-layout", function () {
    return gulp.src([
		"bower_components/jquery/dist/jquery.js",
        "bower_components/bootstrap/dist/js/bootstrap.js",
        "bower_components/vue/dist/vue.js",
        "bower_components/vue-validator/dist/vue-validator.js",
        "Scripts/myPlugin.js"
    ])
	//.pipe($.jshint())                     //jshint實在太繁雜了...
	//.pipe($.jshint.reporter("default"))   //還出報告...瘋了，太慢
	.pipe($.uglify())
	.pipe($.concat("entry.js"))
	.pipe(gulp.dest("Scripts/dist"));
});

//啟用WebServer.
gulp.task("connect", function(){
	connect.server({
		root:"",
		livereload:true
	});
});

gulp.task("html", function(){
	gulp.src("./17Y/*.*")
		.pipe(connect.reload());
});

gulp.task('css-min', function () {
    /*
    <link rel="stylesheet" type="text/css" href="bower_components/semantic-ui/dist/semantic.css" />
    <link rel="stylesheet" type="text/css" href="bower_components/fullcalendar/dist/fullcalendar.css" />
    */
    gulp.src([
                "bower_components/semantic-ui/dist/semantic.css"
                //,"bower_components/fullcalendar/dist/fullcalendar.css"
            ])
        .pipe(cssmin())
        .pipe($.concat("main.min.css"))
        .pipe(gulp.dest('Content/dist'));
});

//gulp.task("default",["connect","watch"]);
gulp.task("default", ["js-layout", "js-layout-dev", "css-min"]);
//gulp.task("default", ["css-min"]);
