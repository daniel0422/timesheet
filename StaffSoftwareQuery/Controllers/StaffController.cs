﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StaffSoftwareQuery.Models;

namespace StaffSoftwareQuery.Controllers
{
    public class StaffController : Controller
    {
        //
        // GET: /Staff/
        StaffEntities db;

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetSoftwareData() {
            using (db = new StaffEntities())
            {
                var es = db.ReadFile
                    .Select(g=>new { 
                        dt=g.ImportDate,
                        rd=g.ReadLine,
                        usr=g.UserName
                    })
                    .OrderBy(x => x.usr)
                    .ToList();
                var jes=Json(new { members = es });
                jes.MaxJsonLength = int.MaxValue;
                return jes;
            }
        }

        [HttpPost]
        public JsonResult Write2DB()
        {
            using (db = new StaffEntities())
            {
                try
                {
                    db.ImportFiles(@"E:\FileToDB\", @"E:\FileToDB\Archive\", "*.txt", "MergeBCPData2");
                    return Json("OK");
                }
                catch (Exception ex) {
                    return Json(ex.ToString());
                }
            }
        }

        public ActionResult About() {
            return View();
        }

    }
}
